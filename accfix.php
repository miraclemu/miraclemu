<?
session_start();
ob_start();
require_once 'engine/functions.php';
$sys = new system();
include 'tpl/header.php';
?>
<div class = "body">
	<?
	 if(isset($_SESSION['user'])) {
		if($sys->fetchMembInfo($_SESSION['user'],"ConnectStat") == 1){
			$sys->fixUser($_SESSION['user']);
			header("location: index.php");
			exit();
		} else {
			header("location: index.php");
			exit();
		}
	 } else {
		 header("location: index.php");
			exit();
	 }
	?>
</div>
<?
include 'tpl/footer.php';
