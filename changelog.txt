[#] Arena Boss spawn time calculation was fixed, instead of seconds we calculated miliseconds....
Now boss must spawn every 2 hours not 2 days.
[#] Pk rankings still useless since, our database don't track total kills... We are working on this.
[#] Added Clear PK option to characters menu.
[#] Added Skill tree reset option to characters menu.
[#] Ancient items were added to boss drops.
[#] Fixed Rankings status indicator, now displays online characters that logged in not all characters of account.
[#] Box Of Might added to game, but only for developers, players will have to download new client patch (after beta). We are sorry
about this, but we can't make it in time before bet release.
[#] Fixed bug when you can use /reset /change commands in crywolf map.
[#] Market-Auction system in the middle of development.
[#] New Website development finished.
[#] New website development started.
[#] Crywolf Experience rates fixed (was lower than general server experience rates.).
[#] Sealed Golden & Silver box - drop fixed.
[#] Blood Castle 1- 8 fixed.
[#] Illusion Temple fixed 1 - 6.
[#] Chaos Castle fixed 1 to 7.
[#] Double Gore Golden Chest 1 to 5 fixed.
[#] Double Gore Silver Chest 1 to 5 fixed.
[#] Imperial Guardian Main Boss & Side boss 1 to 5.
[#] Fixed BoX Of Luck & added to game.
[#] Fixed Boss Erohim.
[#] Boss drops added/fixed missing and buffed all bosses.
[#] Quest items drop fixed (lowered).
[#] Server rankings page now available.
[#] Donations with paypal added and now fully working.
[#] Market system development started.
[#] Online Hours trader -> Credits & Account Experience added, will be available in webshop.
[#] "Webshop" almost done.
[#] Karutan,Aida,Crywolf,Kanturu, Icarus - these maps now have more monsters spawns (not spots but more spawns 2x).
[#] Hearth Of Love added to game with fixed drops.
[#] Arena Boss loot changed to BoK + 2 and drop amount to 3.
[#] ReWrite of Grand Resets system. Please check forum for more information.
[#] Grand resets system added to website.
[#] Account experience formula changed : Resets ^ 1.5 * 5 + 100.
[#] High rr spots added.
[#] Arena Boss was killed.
[#] Medium RR spots were buffed due to their weakness of low damage.
[#] Disabled Req. To enter arena of 2 resets, now everyone can enter it.
[#] Added new spots with packs of 4 monsters for low level/resets players in devias 2 and lost tower 6.
[#] All arena spots ReWorked.
[#] Server Turned Off for maintenance. [ 04 / 20  - 04:00 to 05:00 GTM + 1].
[#] Box Of Kundun +1/+5 drops fixed and added to the game.
[#] Arena Boss Added with 3 random exe items drop.
[#] Changed arena enter req. To 2 resets.
[#] Added Arena Spots for medium resets [~ 20 resets] Inside of arena. Low reset one outside of arena.
[#] Chocolate boxes added to game.
[#] Added Arena spots without item drops.
[#] Account leveling system finished.
[#] Added Arena.
[#] Added more monsters spawns to Lorencia,Noria,Elveland,Devias,Lost Tower.
[#] Cashop will be reworked, only rings/pets/wings are working now, DO NOT BUY PACKAGES.
[#] Server Started.