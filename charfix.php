<? 
session_start(); 
ob_start(); 
require_once 'engine/functions.php';
$sys = new system();
include 'tpl/header.php';
?>
<div class = "body">		
	<?
	 if(isset($_SESSION['user'])) {
		if($sys->fetchMembInfo($_SESSION['user'],"ConnectStat") == 0){
			
			if(isset($_POST['submit'])){
				$sys->fixChar($_SESSION['user'],$_POST['char']);
				$sys->errorHandle("Unstuck done, now you can log in !");
			} 
			
			$sys->fixCharForm($_SESSION['user']);
			//header("location: index.php");
			//exit();
		} else {
			header("location: index.php");
			exit();
		}
	 } else {
		 header("location: index.php");
			exit();
	 }
	?>
</div>
<?
include 'tpl/footer.php';