<?php 
session_start(); 
ob_start(); 
require_once 'engine/functions.php';
$sys = new system();
include 'tpl/header.php';
?>
<div class = "container">	
	
		<span class = "title">Changes # 1 </span> <br />
			2016/04/16 - 2016/04/18<br />
			<ul>
				<li>Account Levels & Experience - Now you earn experience points for makig resets, every 6 levels you earn one Box Of Might.
					All information about how much experience you need and earn can be found in server information page.
				</li>
				<li>Forum development started.</li>
				<li>Chocolate box (red, Pink, Blue) drops fixed.</li>
				<li>Added few extra spots in game.</li>
				<li>Started work on BoK - Box Of Kundun drops.</li>
				<li>Grand resets feature development started.</li>
				<li>Vote system development started.</li>
				<li>Donation system development started.</li>
				<li>Lottery system development started.</li>
				<li>Arena spots added.Monsters dont have any drop.</li>
				<li>Fixed Bug with resets in website when you can make them while your character online.</li>
				<li>Fixed bug when sometimes you can't make reset as DL - Bug Posted by Duke.</li>
				<li>Fixed Bug when DL characters were able do resets for free ( added zen cost ).</li>
				<li class ="notstarted">Small notice for everyone, please use bug tracker to report any bug/issues/suggestions you come up with.</li>
			</ul>
		<hr />
</div> 
<?php
include 'tpl/footer.php';