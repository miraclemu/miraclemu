<?php 
session_start(); 
ob_start(); 
require_once 'engine/functions.php';
$sys = new system();
include 'tpl/header.php';
?>
<div class = "body">	
	
		<ul>
			<li class = 'title'>General Server Information</li>
				<ul>
					<li>Version : Ex803</li>
					<li>Experience : 500 x</li>
					<li>Drop Rates : <span class = "title">30</span> x</li>
					<li>Zen Drop Rates : <span class = "title">30</span> x</li>
					<li>Party Bonus : 120 % / 140 % / 160 % / 180 %</li>
					<li>RF Create Level : 150</li>
					<li>DL Create Level : 250</li>
					<li>MG Create Level : 220</li>
					<li>Elf Buff Max Level : 220</li>
					<li>Max Master Level : 250</li>
					<li>Master Experience : 500 x</li>
				</ul>
				
			<li class = 'title'>Points</li>
				<ul>
					<li>DW Level Up Point : 5</li>
					<li>DK Level Up Point : 5</li>
					<li>FE Level Up Point : 5</li>
					<li>MG Level Up Point : 7</li>
					<li>DL Level Up Point : 7</li>
					<li>SU Level Up Point : 5</li>
					<li>RF Level Up Point : 7</li>
				</ul>
			<li class = 'title'>Monsters spawns / spots</li>
				<ul>
					<li>All maps has increased spawns but most of them are separated. And you have to search best place.</li>
					<li>Arena spots don't have any items drop.</li>
					<li>Arena spots separated to 3 categorys : Low resets[1-30] / Medium resets[30-60] / High resets[60-100]</li>
					<li>Every spot category are stronger than each before and give slightly more experience.</li>
				</ul>
				
			<li class = 'title'>Resets</li>
				<ul>
					<li>Resets enabled : <span class = "title">Yes</span>.</li>
					<li>Reset level : <? echo $sys->cfg['char']['resetLevel']; ?></li>
					<li>Reset zen cost : <? echo number_format($sys->cfg['char']['resetZenCost']); ?> * Reset.</li>
					<li>Stats after reset : burns.</li>
					<li>Points after reset : <? echo number_format($sys->cfg['char']['pointsAfterReset']);?> * Reset + 500 * Grand Reset</li>
					<li>Reset reward : <? echo number_format($sys->cfg['char']['resetReward']);?> Credits</li>
					<li>More informaton
						<a href = "http://www.miraclemu.eu/forum/showthread.php?6-Resets-Grand-Resets-Reborns-explanation&p=6#post6">HERE</a>
					</li>
				</ul>
				
			<li class = 'title'>Grand Resets</li>
				<ul>
					<li>Grand resets enabled : <span class = "title">Yes</span>.</li>
					<li>Grand reset level : <? echo $sys->cfg['char']['grandResetLevel']; ?></li>
					<li>Grand reset zen cost : <? echo number_format($sys->cfg['char']['grandResetZenCost']); ?> * Grand reset.</li>
					<li>Stats after grand reset : burns.</li>
					<li>New and unique feature, each grand reset increase your level up points after reset by <span class = "red">500</span></li>
					<li>Grand reset reward : <? echo number_format($sys->cfg['char']['grandResetReward']);?> Credits</li>
					<li>More informaton
						<a href = "http://www.miraclemu.eu/forum/showthread.php?6-Resets-Grand-Resets-Reborns-explanation&p=6#post6">HERE</a>
					</li>
				</ul>
					
			<li class = 'title'>Reborns</li>
				<ul>
					<li>Information unavailable right now.</li>
					<li>More informaton
						<a href = "http://www.miraclemu.eu/forum/showthread.php?6-Resets-Grand-Resets-Reborns-explanation&p=6#post6">HERE</a>
					</li>
				</ul>
				
			<li class = 'title'>New Features</li>
				<ul>
					<li>Account Levels - <a href = "http://www.miraclemu.eu/forum/showthread.php?8-Account-level-system-explanation&p=8#post8">Guide</a></li>
					<li>New Resets/Grand Resets/Reborns logic - 
					<a href = "http://www.miraclemu.eu/forum/showthread.php?6-Resets-Grand-Resets-Reborns-explanation&p=6#post6">Guide</a></li>
					<li>Box Of Might - <a href = "http://www.miraclemu.eu/forum/showthread.php?109-Box-Of-Might-Guide-And-Drops">Guide</a></li>
					<li>Hall Of Fame - <a href = "#">Guide</a></li>
					<li>Market - <a href = "http://www.miraclemu.eu/forum/showthread.php?107-How-to-use-market">Guide</a></li>
					<li>Achievements - <a href = "#">Guide</a></li>
				</ul>
			
		</ul>


</div>
<?php
include 'tpl/footer.php';