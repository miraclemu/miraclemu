<?php 
session_start(); 
ob_start(); 
require_once 'engine/functions.php';
$sys = new system();
include 'tpl/header.php';
?>
<div class = "body">	
	<h2>MiracleMu Project Rules & Terms Of Agreement</h2>
	<p class = "red ">Make sure you know server rules before you entering the game, Ignorance does not exempt from responsibility</p>
	<ul>
		<li class = "title">General Rules</li>
		
			<ul>
				<li>Treat other players with respect, treat others like you want to be treated.</li>
				<li>Respect all staff members, including the trial administrators/moderators etc.</li>
				<li>Do not start arguments with staff members.</li>
				<li>Any insulting, harassment, racism, or verbal abuse will NOT be tolerated.</li>
				<li>Public shown names such as character names must be proper and can't contain any vulgar content for public.</li>
				<li>Registering on beta version of server you accept that fact that your account data will be wiped after.</li>
				<li class = 'red'>If you found any bug and using / exploiting isntead of reporting your account will be banned.</li>
			</ul>
		<br />
		<br />
		<li class = "title">Software & 3rd party </li>
			<ul>
				<li>Do not use any 3rd party software that is not provided in our downloads section.</li>
				<li>All suspicious actions will be tolerated as a crime and will be reported to particular instances.</li>
			</ul>
		<br />
		<br />
		<li class = "title">Payments & Purchases</li>
			<ul>
				<li>We do not issue refunds for digital products once the order is submitted. We recommend contacting us for assistance if you experience any issues receiving or using our products.</li>
				<li>BY BUYING OUR PRODUCT FROM OUR WEBSITE YOU AGREE THAT, BECAUSE OF THE NATURE OF THE PRODUCTS SOLD,WHICH ARE DIGITAL, THERE ARE NO REFUNDS UNDER ANY CIRCUMSTANCE.</li>
				<li>IF YOU SUBMIT ANY PAYMENT YOU AGREE THAT YOU WILL NOT DISPUTE, ASK FOR A PARTIAL REFUND, OR A FULL REFUND.</li>
				<li>IF YOU DO NOT AGREE TO THE ABOVE DO NOT SUBMIT ANY PAYMENT.</li>
				<li>There is only one exception for purchases, if you bought something by mistake or a wrong item because of lack of knowledge. please contact us using the support email which is: suppmiraclemu@gmail.com with your issue explained. If your issue report will be sent in less than 12 hours after your purchase we will make sure that you get a refund.</li>
			</ul>
			<br />
		<br />
		<li class = "title">In Game Rules</li>
			<ul>
				<li>Spawn Kill limited to 5 times in a row, after you must give other player time not less than 1 minute before you can attack him again.</li>
				<li>PvP in monster spawn areas are not allowed.</li>
				<li>Overtaking other player's leveling spots are not allowed, be friendly and send him party invitation.</li>
				<li>We are not responsible for your trades, so please don't ask us for refunds if you noticed that your deal was not fair.</li>
				<li>We are not responsible for items that dropped in world or from boxes, if other player takes it before you, he is quicker.</li>
				<li class = 'red'>Use English language in /post /global chats. In private messages feel free to speak whatever language you wan't</li>
			</ul>
			<br />
		<br />
	</ul>
</div>
<?php
include 'tpl/footer.php';