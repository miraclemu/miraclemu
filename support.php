<?php 
session_start(); 
ob_start(); 
require_once 'engine/functions.php';
$sys = new system();
include 'tpl/header.php';
?>
<div class = "body">	
	<h2>Bug Tracker Form</h2>
	<?php if(!isset($_SESSION['user'])) { ?>
	<p class = "title2"> Please log in to use Bug Tracker </p>
	<?php } else { ?>
	<?php if(isset($_POST['submit'])){
		
		$sys->sendRepot($_POST['name'],$_POST['desc'],$_POST['category'],$_POST['url'],$_SESSION['user']);
		echo "<h2>Thank you for your report</h2>";
	} ?>

	<form action = "" method = "POST">
	
	<br /><br />Issue Title<br />
		<input type = "text" name = 'name' placeholder = "Report Issue Title" class = "input" required/>
		
	<br /><br />Full Issue Description<br />
		<textarea type = "text" name = 'desc' class = "input" style = "width:90%;height:150px;" required>Enter your issue here.</textarea>
	
	<br /><br />Image or Video proof url.<br />
		<input type = "text" name = 'url' placeholder = "Image or video url." class = "input"required/>
		
	<br /><br />
	<input type = "radio" name = 'category' value = 'Website' required /> Website bug/issue <br />
	<input type = "radio" name = 'category' value = 'InGame' required/> In Game bug/issue <br />
	
	<br /> <br />
	<input type = "submit" name = 'submit' value = "Send Report" class = "ok"/>
	</form>

	<?php } ?>
</div>
<?php
include 'tpl/footer.php';