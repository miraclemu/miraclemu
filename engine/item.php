<?
class Items
{
  public $id;
  public $group;
  public $level;
  public $option;
  public $skill;
  public $luck;
  public $excellent;
  public $durability;
  public $sockets;
  public $serial1;
  public $serial2;
  public $harmony_type;
  public $harmony_level;
  public $pvp;
  public $hex;
  public $ancient;

  function __construct($hex)
  {
    // ???????????? ???????? ?????????? ?? ?????????
    $this->init($hex);
    // ?????? id ????
    $this->id = $this->dehex(substr($hex, 0, 2));
    // ?????? ?????? ????
    $this->group = $this->dehex(substr($hex, 18, 1));
    // ?????? ???? ?? skill
    $opt = $this->dehex(substr($hex, 2, 2));
    if ($opt >= 128){
		$this->skill = 1;
	}
    // ?????? level
    if ($this->skill == 1) {
		 $opt -= 128;
	}
    $this->level = (integer)($opt / 8);
    // ?????? luck
    $opt = (integer)($opt % 8);
    if ($opt > 3) {
		$this->luck = 1;
	}
    // ?????? excellent
    if ($opt > 3) {
		$opt -= 4;
	}
    $ex = $this->dehex(substr($hex, 14, 2));
    if ($ex >= 128)
    {
      $ex -= 128;
      $this->id += 256;
    }
    if ($ex > 63)
    {
      $ex -= 64;
      $opt += 4;
    }
    if ($ex >= 32) { $ex -= 32; $this->excellent += 32; }
    if ($ex >= 16) { $ex -= 16; $this->excellent += 16; }
    if ($ex >= 8) { $ex -= 8; $this->excellent += 8; }
    if ($ex >= 4) { $ex -= 4; $this->excellent += 4; }
    if ($ex >= 2) { $ex -= 2; $this->excellent += 2; }
    if ($ex >= 1) { $ex -= 1; $this->excellent += 1; }
    // ?????? option
    $this->option = $opt + $ex;
    // ?????? harmony
    $this->harmony_type = $this->dehex(substr($hex, 20, 1));
    $this->harmony_level = $this->dehex(substr($hex, 21, 1));
    // ?????? pvp
    $this->pvp = substr($hex, 19, 1);
    // ?????? sockets
    $this->sockets = substr($hex, 22, 10);
    // ?????? durability
    $this->durability = $this->dehex(substr($hex, 4, 2));
    // ?????? serial1
    $this->serial1 = substr($hex, 6, 8);
    // ?????? serial2
    $this->serial2 = substr($hex, 32, 8);
    // ?????? ancient
    $this->ancient = $this->dehex(substr($hex, 17, 1));
  }
  private function init($hex)
  {
    $this->id = 0;
    $this->group = 0;
    $this->level = 0;
    $this->option = 0;
    $this->skill = 0;
    $this->luck = 0;
    $this->excellent = 0;
    $this->durability = 0;
    $this->sockets = 0;
    $this->serial1 = 0;
    $this->serial2 = 0;
    $this->harmony_type = 0;
    $this->harmony_level = 0;
    $this->pvp = 0;
    $this->ancient = 0;
    $this->hex = $hex;
  }
  protected function dehex($hex)
  {
    return hexdec($hex);
  }
  public function getId()
  {
    return $this->id;
  }
  public function getGroup()
  {
    return $this->group;
  }
  public function getLevel()
  {
    return $this->level;
  }
  public function getOption()
  {
    return $this->option;
  }
  public function getSkill()
  {
    return $this->skill;
  }
  public function getLuck()
  {
    return $this->luck;
  }
  public function getExcellent()
  {
    return $this->excellent;
  }
  public function getDurability()
  {
    return $this->durability;
  }
  public function getSockets()
  {
    return $this->sockets;
  }
  public function getHarmonyType()
  {
    return $this->harmony_type;
  }
  public function getHarmonyLevel()
  {
    return $this->harmony_level;
  }
  public function getPvp()
  {
    return $this->pvp;
  }
  public function getSerial1()
  {
    return $this->serial1;
  }
  public function getSerial2()
  {
    return $this->serial2;
  }
  public function getAncient()
  {
    return $this->ancient;
  }
  public function getHex()
  {
    return $this->hex;
  }
}