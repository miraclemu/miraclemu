<?php 
require_once 'item.php';
class system extends Items{
	
	protected $webConn;
	protected $gameConn;
	public $cfg;
	//
	
	//
	function __construct(){
		
		
		// set variable
		$this->webConn = $this->webConn();
		$this->gameConn = $this->gameConn();
		
		// include configs
		include 'engine/config.php';
		$this->cfg = $conf;
	}
	
	function serverStatus($ip,$port){
		//$check = fsockopen($ip,$port);
		$check = false;
		return $check;
	}
	
	function PlayersOnline(){
		
		$db = $this->gameConn;
		$sql = "SELECT ConnectStat FROM MEMB_STAT WHERE ConnectStat = '1'";
		$stmt = sqlsrv_query($db, $sql, array(), array( "Scrollable" => SQLSRV_CURSOR_KEYSET ));
		$row_count = sqlsrv_num_rows($stmt);
	
		echo $row_count + 10;
		
	}
	
	function xShopData($username,$data){
		$db = $this->gameConn;
		$sql = "SELECT * FROM CashShopData WHERE 
			AccountID='".$this->clean($username)."'";
		$stmt = sqlsrv_query($db,$sql);
		$result = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC);
		
		return $result[$data];
	}
	
	function xpForLevel($level){
		$formula = round(pow($level + 1,1.8) * 8 + 1876);
		return $formula;
	}
	
	function xpForReset($reset){
		$formula = round(pow($reset,1.5) * 5 + 100);
		return $formula;
	}	
	
	function xpForGrandReset($reset){
		$formula = round($reset * 1000);
		return $formula;
	}
	
	function webShopZenForm($user){
		$db = $this->gameConn;
		
		$sql = "SELECT * FROM Character WHERE AccountID = '".$this->clean($user)."'";
		$stmt = sqlsrv_query($db, $sql, array(), array("Scrollable"=>SQLSRV_CURSOR_KEYSET));
		echo "<p>Select character</p>";
		echo "<form action = '' method = 'POST'>";
		echo "<select name = 'char' class = 'input'>";
		echo "<option value = ''>-----</option>";
			while($row=sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
					echo "<option value = '".$row['Name']."'>".$row['Name']."</option>";
			}
		echo "</select>";
		echo "<p>Enter ammount of zen you wanna buy</p>";
		echo "<input type = 'number' name = 'ammount' value = '100000' class = 'input' max = '2000000000' min = '100000' required/>";
		
		echo "<p><input type = 'submit' name = 'submit' value = 'Buy' class = 'ok'/></p>";
		echo "</form>";
	}
	
	function fixCharForm($user){
		$db = $this->gameConn;
		
		$sql = "SELECT * FROM Character WHERE AccountID = '".$this->clean($user)."'";
		$stmt = sqlsrv_query($db, $sql, array(), array("Scrollable"=>SQLSRV_CURSOR_KEYSET));
		echo "<p>Select character</p>";
		echo "<form action = '' method = 'POST'>";
		echo "<select name = 'char' class = 'input'>";
		echo "<option value = ''>-----</option>";
			while($row=sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
					echo "<option value = '".$row['Name']."'>".$row['Name']."</option>";
			}
		echo "</select>";
		echo "<p><input type = 'submit' name = 'submit' value = 'Unstuck' class = 'ok'/></p>";
		echo "</form>";
	}
	
	
	
	function webShopResetsForm($user){
		$db = $this->gameConn;
		
		$sql = "SELECT * FROM Character WHERE AccountID = '".$this->clean($user)."'";
		$stmt = sqlsrv_query($db, $sql, array(), array("Scrollable"=>SQLSRV_CURSOR_KEYSET));
		echo "<p>Select character</p>";
		echo "<form action = '' method = 'POST'>";
		echo "<select name = 'char' class = 'input'>";
		echo "<option value = ''>-----</option>";
			while($row=sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
					echo "<option value = '".$row['Name']."'>".$row['Name']."</option>";
			}
		echo "</select>";
		echo "<p>Enter ammount of resets you wanna buy</p>";
		echo "<input type = 'number' name = 'ammount' value = '1' class = 'input' max = '100' min = '1' required/>";
		
		echo "<p><input type = 'submit' name = 'submit' value = 'Buy' class = 'ok'/></p>";
		echo "</form>";
	}

	function buyResets($character,$account,$ammount) {
		$db = $this->gameConn;
		$cost = $ammount * 25;
		$sql = "SELECT * FROM Character 
			WHERE 
			Name ='".$this->clean($character)."'";
		$query = sqlsrv_query($db,$sql);
		$fetch = sqlsrv_fetch_array($query,SQLSRV_FETCH_ASSOC);
		
		if($this->xShopData($account,"WCoinC") >= $cost){
			
			if($fetch['ResetCount'] + $ammount <= 100) {
				$summRR = $fetch['ResetCount'] + $ammount;
				
				$newPoints = $summRR * 500 + (($fetch['ResetCount'] + 1) * 500 * $fetch['GrandResetCount']);
				sqlsrv_query($db,"UPDATE CashShopData SET WCoinC = WCoinC - $cost WHERE AccountID = '".$account."'");
				
				if($fetch['Class'] != 64 || $fetch['Class'] != 66){
					sqlsrv_query($db,"UPDATE Character SET cLevel = 1 , 
					ResetCount = ResetCount + $ammount , 
					LevelUpPoint = $newPoints,
					Strength = 35,Dexterity = 35,
					Vitality = 35,Energy = 35,
					Leadership = 0, Experience = '0'
					WHERE Name = '".$character."'")or die(print_r(sqlsrv_errors(), true));
				
				} else {
					sqlsrv_query($db,"UPDATE Character SET cLevel = 1 , 
					ResetCount = ResetCount + $ammount , 
					LevelUpPoint = $newPoints,
					Strength = 35,Dexterity = 35,
					Vitality = 35,Energy = 35,
					Leadership = 35, Experience = '0'
					WHERE Name = '".$character."'")or die(print_r(sqlsrv_errors(), true));
				}
				
				$this->errorHandle("You bought $ammount Resets for character : $character");
			} else {
				$this->errorHandle("Enter lower ammount");
			}
			
		}else {
			$this->errorHandle("not enough credits");
		}
		
	}
	
	function buyZen($character,$account,$ammount) {
		$db = $this->gameConn;
		$cost = $ammount / 100000;
		$sql = "SELECT * FROM Character 
			WHERE 
			Name ='".$this->clean($character)."'";
		$query = sqlsrv_query($db,$sql);
		$fetch = sqlsrv_fetch_array($query,SQLSRV_FETCH_ASSOC);
		
		if($this->xShopData($account,"WCoinC") >= $cost && $cost >= 1){
			
			if($fetch['Money'] + $ammount <= 2000000000) {
				$summRR = $fetch['ResetCount'] + $ammount;
				
				sqlsrv_query($db,"UPDATE CashShopData SET WCoinC = WCoinC - $cost WHERE AccountID = '".$account."'");
				sqlsrv_query($db,"UPDATE Character SET Money = Money + $ammount WHERE Name = '".$character."'");
				
				
				$this->errorHandle("You bought $ammount Zen for character : $character");
			} else {
				$this->errorHandle("Enter lower ammount");
			}
			
		}else {
			$this->errorHandle("not enough credits");
		}
		
	}
	
	function fixChar($account,$character) {
		$db = $this->gameConn;
		
		sqlsrv_query($db,"UPDATE Character SET MapNumber = 0 , MapPosX = 148 , MapPosY = 127 WHERE Name = '".$character."'");
		
	}
	
	function register($username,$password,$passwordC,$email) {
		
		$Wdb = $this->webConn;
		$Gdb = $this->gameConn;
		
		$sql = "SELECT * FROM dbo.users WHERE username = '".$this->clean($username)."' AND password = '".$this->clean($password)."'";
		$stmt = sqlsrv_query($Wdb, $sql, array(), array("Scrollable"=>SQLSRV_CURSOR_KEYSET));
		$row_count = sqlsrv_num_rows($stmt);
		
		if($row_count <= 0) {
			if($password == $passwordC){
				if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$gameQuery = "INSERT INTO [MEMB_INFO] (memb___id,memb__pwd,memb_name,sno__numb,mail_addr,appl_days,modi_days,out__days,true_days,mail_chek,bloc_code,ctl1_code,fpas_ques,fpas_answ) 
					VALUES ('".$this->clean($username)."','".$this->clean($password)."','EMU','0','".$this->clean($email)."','01/01/2006','01/01/2006','01/01/2006','01/01/2006','1','0','0','','')";
					$webQuery = "INSERT INTO [users] (username,password,email,xp,lvl,box) 
					VALUES ('".$this->clean($username)."','".$this->clean($password)."','".$this->clean($email)."','0','1','0')";
					// website
					sqlsrv_query($Wdb,$webQuery)or die(print_r(sqlsrv_errors(), true));
					// server
					sqlsrv_query($Gdb,$gameQuery)or die(print_r(sqlsrv_errors(), true));
					$result = "Account : $username created. Now you can log in !";
				} else {
					$result = "Email is invalid, please use valid email.";
				}
			} else {
				$result = "Passowrds don't match , please double check spelling.";
			}
		} else {
			$result = "Account name already in use, please select new one.";
		}
		
		echo $result;

	}
	
	function fetchMembInfo($name,$data){
		$db = $this->gameConn;
		$sql = "SELECT * FROM MEMB_STAT WHERE memb___id='".$name."'";
		$stmt = sqlsrv_query($db,$sql)or die(print_r(sqlsrv_errors(), true));
		
		$result = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC);
		return $result[$data];
	}
	
	function onlineHoursTrade($name){

		echo '
			<form action = "" method = "post">
		';

				echo "<input type = 'hidden' name = 'ammount' value = '".$this->fetchMembInfo($name,"OnlineHours")."'/>";
		echo '
				
				<input type = "submit" name = "submit" value = "Trade" class= "ok"/>
			</form>
		
		';
		
	}
	
	function fixuser($acc){
		$db = $this->gameConn;
		sqlsrv_query($db,"UPDATE MEMB_STAT SET ConnectStat = 0 WHERE memb___id = '".$acc."'");
	}
	
	function tradeHours($name,$ammount){
			$db = $this->gameConn;
			$db2 = $this->webConn;
		$credits = $ammount * $this->cfg['char']['onlineHoursRatio'];
		
		if($this->fetchMembInfo($name,"DisConnectTM") > $this->fetchMembInfo($name,"ConnectTM")){
			
			if(!empty($ammount)){
				
					if($this->fetchMembInfo($name,"OnlineHours") == $ammount){
						
					sqlsrv_query($db,"UPDATE CashShopData SET WCoinC = WCoinC + $credits WHERE AccountID = '".$name."'");
					
					sqlsrv_query($db,"UPDATE MEMB_STAT SET OnlineHours = 0 , ConnectTM = getdate() , DisConnectTM = getdate() WHERE memb___id = '".$name."'")or die(print_r(sqlsrv_errors(), true));;
						 
					$newXP = $ammount * 25;
					
					sqlsrv_query($db2,"UPDATE users SET xp = xp + $newXP WHERE username = '".$name."'");	
					
						$this->errorHandle("successfully traded $ammount Hours to $credits Credits");
						
						$file = 'hours_log.txt';
						// Open the file to get existing content
						$current = file_get_contents($file);
						// Append a new person to the file
						$current .= "Account $name traded $ammount hours ".date("Y-m-d H:i:s")."\n";
						// Write the contents back to the file
						file_put_contents($file, $current);	
						
							header("Location: webshop.php?action=hours");
						
					} else {
						
						$this->errorHandle("Not enough hours.");
						
					}
					
			} else {
				$this->errorHandle("Please enter value");
			}
		} else {
			$this->errorHandle("Contact Adminsitrator");
		}
	}
	
	function beta($old,$new){
		$db2 = $this->webConn;
	}
	
	function isOnline($name){
		$db = $this->gameConn;
			$sql = "SELECT ConnectStat FROM MEMB_STAT WHERE memb___id = '".$name."' AND ConnectStat = '1'";
			$stmt = sqlsrv_query($db, $sql, array(), array( "Scrollable" => SQLSRV_CURSOR_KEYSET ));
			$row_count = sqlsrv_num_rows($stmt);
			
			if($row_count > 0){
				return true;
			} else {
				return false;
			}

			
	}
	
	function MakeReset($name,$reset){
		$db = $this->gameConn;
		$sql = "SELECT * FROM Character 
			WHERE Name='".$this->clean($name)."'";
		$stmt = sqlsrv_query($db, $sql, array(), array())or die(print_r(sqlsrv_errors(), true));
		$fetch = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC);
		
		$zenCost = $reset * $this->cfg['char']['resetZenCost'] + $fetch['GrandResetCount'] * $this->cfg['char']['resetZenCost'];
		$newPoints = $reset * $this->cfg['char']['pointsAfterReset'] + (($fetch['ResetCount'] + 1) * 500 * $fetch['GrandResetCount']);
		
		if($fetch['cLevel'] >= $this->cfg['char']['resetLevel'] && $fetch['Money'] >= $zenCost) {
				if($fetch['Class'] != 64 || $fetch['Class'] != 66){
					sqlsrv_query($db,"UPDATE Character SET cLevel = 1 , 
					ResetCount = ResetCount + 1 , 
					LevelUpPoint = $newPoints,
					Strength = 35,Dexterity = 35,
					Vitality = 35,Energy = 35,
					Leadership = 0, Money = Money - ".$zenCost.", Experience = '0'
					WHERE Name = '".$name."'")or die(print_r(sqlsrv_errors(), true));
				header("Location: characters.php?action=reset");
				} else {
					sqlsrv_query($db,"UPDATE Character SET cLevel = 1 , 
					ResetCount = ResetCount + 1 , 
					LevelUpPoint = $newPoints,
					Strength = 35,Dexterity = 35,
					Vitality = 35,Energy = 35,
					Leadership = 35, Money = Money - ".$zenCost.", Experience = '0'
					WHERE Name = '".$name."'")or die(print_r(sqlsrv_errors(), true));
				header("Location: characters.php?action=reset");
				}
			
			sqlsrv_query($db,"UPDATE CashShopData SET WCoinC = WCoinC + 5 WHERE AccountID = '".$_SESSION['user']."'");
			$this->giveExp($reset);
		} else {
			$this->errorHandle("Character does not meet requirements");
		}	
	}
	
	function MakeGrandReset($name,$reset){
		$db = $this->gameConn;
		$sql = "SELECT * FROM Character 
			WHERE Name='".$this->clean($name)."'";
		$stmt = sqlsrv_query($db, $sql, array(), array())or die(print_r(sqlsrv_errors(), true));
		$fetch = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC);
		
		if($fetch['GrandResetCount'] == NULL) {
				$GrandReset = 1;		
		} else {
			$GrandReset = $fetch['GrandResetcount'] + 1;
		}
		
		$zenCost = $GrandReset * $this->cfg['char']['grandResetZenCost'];
		$newPoints = $GrandReset * 50000;
		if($fetch['cLevel'] >= $this->cfg['char']['grandResetLevel'] && $fetch['Money'] >= $zenCost && $fetch['ResetCount'] >= $this->cfg['char']['grandResetReset']) {
				if($fetch['Class'] != 64){
					sqlsrv_query($db,"UPDATE Character SET cLevel = 1 , 
					ResetCount = 0, 
					LevelUpPoint = ".$newPoints.",
					Strength = 35,Dexterity = 35,
					Vitality = 35,Energy = 35,
					Leadership = 0, Money = Money - ".$zenCost.", Experience = '0',GrandResetCount = '".$GrandReset."'
					WHERE Name = '".$name."'")or die(print_r(sqlsrv_errors(), true));
				header("Location: characters.php?action=reset");
				} else {
					sqlsrv_query($db,"UPDATE Character SET cLevel = 1 , 
					ResetCount = 0, 
					LevelUpPoint = ".$newPoints.",
					Strength = 35,Dexterity = 35,
					Vitality = 35,Energy = 35,
					Leadership = 35, Money = Money - ".$zenCost.", Experience = '0',GrandResetCount = '".$GrandReset."'
					WHERE Name = '".$name."'")or die(print_r(sqlsrv_errors(), true));
				header("Location: characters.php?action=reset");
				}
			
			sqlsrv_query($db,"UPDATE CashShopData SET WCoinC = WCoinC + 500 WHERE AccountID = '".$_SESSION['user']."'");
			$this->giveExp2($GrandReset);	

		} else {
			$this->errorHandle("Character does not meet requirements");
		}		
	}
	
	function giveExp($reset){
		
		$db2 = $this->webConn;
		$xp = $this->xpForReset($reset);
		$lvl = $this->fetchUser("lvl");
		sqlsrv_query($db2,"UPDATE users SET xp = xp + $xp WHERE username = '".$_SESSION['user']."'");
		
			
	}
	
	function giveExp2($reset){
		
		$db2 = $this->webConn;
		$xp = $this->xpForGrandReset($reset);
		$lvl = $this->fetchUser("lvl");
		sqlsrv_query($db2,"UPDATE users SET xp = xp + $xp WHERE username = '".$_SESSION['user']."'");
		
			
	}
	
	function levelUp() {
		$db2 = $this->webConn;
		$db = $this->gameConn;

		if($this->fetchUser("lvl") >= 3) {
				sqlsrv_query($db,"UPDATE AccountCharacter SET ExtWarehouse = 1 WHERE Id = '".$_SESSION['user']."'");
			}
		
		if($this->fetchUser("xp") >= $this->xpForLevel($this->fetchUser("lvl"))) {
			
			// calculate new xp 
			$newXp = $this->fetchUser("xp") - $this->xpForLevel($this->fetchUser("lvl"));
			
			// update db
			sqlsrv_query($db2,"UPDATE users SET xp = $newXp, lvl = lvl + 1 WHERE username = '".$_SESSION['user']."'")or die(print_r(sqlsrv_errors(), true));
			
			// give box every 6 levels
			if ($this->fetchUser("lvl") % 6 == 0) {
				//sqlsrv_query($db2,"UPDATE users SET box = box + 1 WHERE username = '".$_SESSION['user']."'")or die(print_r(sqlsrv_errors(), true));
				sqlsrv_query($db,"UPDATE CashShopData SET WCoinC = WCoinC + 20 WHERE AccountID = '".$_SESSION['user']."'");
				$serial = substr(hexdec(mt_rand(10,13654236547)),0,3);
				$item = "340000FFFFF".$serial."0000E000FFFFFFFFFF";
				sqlsrv_query($db2,"INSERT INTO online_warehouse (AccountID,item_hex) VALUES ('".$_SESSION['user']."','".$item."')")or die(print_r(sqlsrv_errors(), true));
			}
			
			sqlsrv_query($db,"UPDATE warehouse SET Money = Money + ". 2000000 * ($this->fetchUser("lvl")) ." WHERE AccountID = '".$_SESSION['user']."'")or die(print_r(sqlsrv_errors(), true));
			
			
			// reload page
			Header("location: index.php");
			$this->errorHandle("Congrats with level up !");
			exit();
			
		}
	}
	
	
	function showCharacters($accountName){
		$db = $this->gameConn;
		$sql = "SELECT * FROM Character 
			WHERE 
			AccountID='".$this->clean($accountName)."'";
		$stmt = sqlsrv_query($db, $sql, array(), array("Scrollable"=>SQLSRV_CURSOR_KEYSET));
		$i = 0;
			while($key = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
				echo "<tr>";
				$i++;
					echo "<td>$i</td>";
					echo "<td>".$key['Name']."</td>";
					echo "<td>".$key['cLevel']."</td>";
					echo "<td>".$key['ResetCount']."</td>";
					echo "<td>".number_format($key['Money'])."</td>";
					echo "<td>". number_format($this->cfg['char']['resetZenCost'] * ($key['ResetCount'] + 1) + $key['GrandResetCount'] * $this->cfg['char']['resetZenCost']) ."</td>";
					echo "<td>".$this->xpForReset($key['ResetCount'] + 1)."</td>";
					if($key['cLevel'] >= $this->cfg['char']['resetLevel'] && 
						$key['Money'] >= ($this->cfg['char']['resetZenCost'] * $key['ResetCount'] + 1) && 
							$key['ResetCount'] < $this->cfg['char']['grandResetLevel']) {
								
						$nextReset = $key['ResetCount'] + 1;
						echo "<td><a href = 'characters.php?action=reset&name=".$key['Name']."&NextReset=".$nextReset."'>Reset Character</a></td>";
					} else {
						echo "<td>Character does not meet requirements.</td>";
					}
				echo "</tr>";
			}

	}
	
	function showCharacters3($accountName){
		$db = $this->gameConn;
		$sql = "SELECT * FROM Character 
			WHERE 
			AccountID='".$this->clean($accountName)."'";
		$stmt = sqlsrv_query($db, $sql, array(), array("Scrollable"=>SQLSRV_CURSOR_KEYSET));
		
		$i = 0;
			while($key = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
				
			
				echo "<tr>";
				$i++;
					echo "<td>$i</td>";
					echo "<td>".$key['Name']."</td>";
					echo "<td>".$key['cLevel']."</td>";
					echo "<td>".$key['ResetCount']."</td>";
					echo "<td>".number_format($key['Money'])."</td>";
					echo "<td>".number_format($this->cfg['char']['grandResetZenCost'] * ($key['GrandResetCount'] + 1))."</td>";
					echo "<td>".$this->xpForGrandReset($key['GrandResetCount'] + 1) ."</td>";
					if($key['cLevel'] >= $this->cfg['char']['grandResetLevel'] && 
						$key['ResetCount'] >= $this->cfg['char']['grandResetReset'] && 
							$key['Money'] >= $this->cfg['char']['grandResetZenCost'] * ($key['GrandResetCount'] + 1)) {
								
						$nextReset = $key['GrandResetCount'] + 1;
						echo "<td><a href = 'characters.php?action=Greset&name=".$key['Name']."&NextGReset=".$nextReset."'>Grand Reset</a></td>";
					} else {
						echo "<td>Character does not meet requirements.</td>";
					}
				echo "</tr>";
			}

	}
		
	function showCharacters2($accountName){
		$db = $this->gameConn;
		$sql = "SELECT * FROM Character 
			WHERE 
			AccountID='".$this->clean($accountName)."'";
		$stmt = sqlsrv_query($db, $sql, array(), array("Scrollable"=>SQLSRV_CURSOR_KEYSET));
		$i = 0;
			while($key = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
				echo "<tr>";
				echo "<form action = '' method ='post' id = '".$key['Name']."'>";
				$i++;
					echo "<td>$i</td>";
					echo "<td>".$key['Name']."</td>";
					echo "<td>".$key['LevelUpPoint']."</td>";
					echo "<input type = 'text' name = 'name' value = '".$key['Name']."' hidden/>";
					echo "<td>".$key['Strength']."</td>";
						echo "<td><input type = 'number' name = 'str' value = '0' class = '' style = 'width:40px;' max = '65000' min = '0'/></td>";
					echo "<td>".$key['Dexterity']."</td>";
						echo "<td><input type = 'number' name = 'dex' value = '0' class = '' style = 'width:40px;' max = '65000' min = '0'/></td>";
					echo "<td>".$key['Vitality']."</td>";
						echo "<td><input type = 'number' name = 'vit' value = '0' class = '' style = 'width:40px;' max = '65000' min = '0'/></td>";
					echo "<td>".$key['Energy']."</td>";
						echo "<td><input type = 'number' name = 'ene' value = '0' class = '' style = 'width:40px;' max = '65000' min = '0'/></td>";
					if($key['Class'] == 64) {
						echo "<td>".$key['Leadership']."</td>";
							echo "<td><input type = 'number' name = 'cmd' value = '0'  class = '' style = 'width:40px;' min = '0' max = '6500'/></td>";
					} else {
						echo "<td>".$key['Leadership']."</td>";
						echo "<td><input type = 'number' name = 'cmd' value = '0' style = 'width:40px;' class = '' disabled/></td>";
					}
					
					echo "<td>
						<input type = 'submit' value = 'ADD' name = 'submit'/>
					</td>";
				echo "</tr></form>";
			}
	}
		
	function showCharacters4($accountName){
		$db = $this->gameConn;
		$sql = "SELECT * FROM Character 
			WHERE 
			AccountID='".$this->clean($accountName)."'";
		$stmt = sqlsrv_query($db, $sql, array(), array("Scrollable"=>SQLSRV_CURSOR_KEYSET));
		$i = 0;
			while($key = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
				echo "<tr>";
				echo "<form action = '' method ='post' id = '".$key['Name']."'>";
				$i++;
					echo "<td>$i</td>";
					echo "<td>".$key['Name']."</td>";
					echo "<td>".$key['cLevel']."</td>";
					echo "<td>".$key['Money']."</td>";
					echo "<input type = 'text' name = 'name' value = '".$key['Name']."' hidden/>";
					if($key['Money'] < 100000000 || $key['PkLevel'] == 3){
					echo "<td>Character does not meet requirements.</td>";
					} else {
					echo "<td>
						<input type = 'submit' value = 'Clear' name = 'submit'/>
					</td>";
					}
				echo "</tr></form>";
			}
	}
	
	function pkClear($char){
		$db = $this->gameConn;
		$sql = "UPDATE Character SET PkLevel = '3', Money = Money - 100000000 WHERE
			Name ='".$this->clean($char)."'";
		sqlsrv_query($db,$sql)or die(print_r(sqlsrv_errors(), true));;
		header("Location: characters.php?action=pkclear");
		
	}
	
	function addStats($name,$value1,$value2,$value3,$value4,$value5){
		if(!is_numeric($value1) && !is_numeric($value2) && !is_numeric($value3) && !is_numeric($value4) && !is_numeric($value5)) {
			
			//header("Location:characters.php?action=add");
			$this->errorHandle("Enter number");
			
		} else {
		
			if($this->getCharDataV2($name,"LevelUpPoint") >= $value1 + $value2 + $value3 + $value4 + $value5){
				
				$db = $this->gameConn;
				if($this->getCharData($name,"Strength") + $value1 <= $this->cfg['char']['max_stats'] && $value1 > 0){
					sqlsrv_query($db,"UPDATE Character SET Strength = Strength + $value1 ,LevelUpPoint = LevelUpPoint - $value1 WHERE Name = '".$name."'");
				}
				if($this->getCharData($name,"Dexterity") + $value2 <= $this->cfg['char']['max_stats'] && $value2 > 0){
					sqlsrv_query($db,"UPDATE Character SET Dexterity = Dexterity + $value2, LevelUpPoint = LevelUpPoint - $value2 WHERE Name = '".$name."'");
				}
				if($this->getCharData($name,"Vitality") + $value3 <= $this->cfg['char']['max_stats'] && $value3 > 0){
					sqlsrv_query($db,"UPDATE Character SET Vitality = Vitality + $value3, LevelUpPoint = LevelUpPoint - $value3 WHERE Name = '".$name."'");
				}
				if($this->getCharData($name,"Energy") + $value4 <= $this->cfg['char']['max_stats'] && $value4 > 0){
					sqlsrv_query($db,"UPDATE Character SET Energy = Energy + $value4, LevelUpPoint = LevelUpPoint - $value4 WHERE Name = '".$name."'");
				}
				if($this->getCharData($name,"Leadership") + $value5 <= $this->cfg['char']['max_stats'] && $value5 > 0){
					sqlsrv_query($db,"UPDATE Character SET Leadership = Leadership + $value5 ,LevelUpPoint = LevelUpPoint - $value5 WHERE Name = '".$name."'");
					
				}
				
			} else {
				$this->errorHandle("You don't have enough points");
			}
			
		}
	}
	
	function listItems($user){
		$db = $this->gameConn;
		$sql = "SELECT * FROM warehouse WHERE AccountID = '".$user."'";
		$stmt = sqlsrv_query($db, $sql)or die(print_r(sqlsrv_errors(), true));
		$fetch = sqlsrv_fetch_array($stmt);
		
		$items = strtoupper(bin2hex($fetch['Items']));
		$items_split = str_split($items,32);
		$i = -1;
		echo "<table width = '100%' celpading = '0' cellspacing = '0' class = 'stats4' border = '1'>";
		while($i++ < 119){
			$new_item = preg_replace("/FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF/","",$items_split);
			if(!empty($new_item[$i])){
				$item = new Items($new_item[$i]);
				$level = $item->getLevel();
				$type = $item->getGroup();
				$id = $item->getId();
				$exe = $item->getExcellent();
				$skill = $item->getSkill();
				$luck = $item->getLuck();
				$opt = $item->getOption();
				$exe_new = "";
				$ac = $item->getAncient();
				$ac_new = "";
				$itemname = $this->getItemName($new_item[$i]);
				switch($ac)
				{
				case 5: case 9:
					{
					if( /*Warrior Leather Set*/
						(($type==7) && ($id==5)) ||          
						(($type==8) && ($id==5)) ||
						(($type==9) && ($id==5)) ||
						(($type==10) && ($id==5)) ||
						(($type==11) && ($id==5)) ||
						(($type==12) && ($id==8)) ||
						(($type==2) && ($id==1))
					) {$itemname = 'Warrior '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Strength +10<br />Incr. attack rate +10<br />Incr. maximum AG +20<br />AG increase Rate + 5<br />Increase Defense + 20<br />Increase agility +10<br />Critical damage rate + 5%<br />Excellent damage rate + 5%<br />Increase strength + 25</font>";}
					elseif( /*Hyperion Bronze Set*/
						(($type==8) && ($id==0)) ||
						(($type==9) && ($id==0)) ||
						(($type==11) && ($id==0)) 
					) {$itemname = 'Hyperion '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Energy + 15<br />Increase Agility + 15<br />Increase skill attacking rate +20<br />Increase Mana + 30</font>";}
					elseif( /*Eplete Scale Set*/
						(($type==7) && ($id==6)) ||
						(($type==8) && ($id==6)) ||
						(($type==9) && ($id==6)) ||
						(($type==6) && ($id==9)) ||
						(($type==12) && ($id==2)) 
					) {$itemname = 'Eplete '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase atack skill + 15<br />Increase atack + 50<br />Increase Wizardry dmg + 5%<br />Maximum HP +50<br />Increase maximum AG +30<br />Incr. Critical damage rate +10%<br />Excellent damage rate +10</font>";}
					elseif( /*Garuda Brass Set*/
						(($type==8) && ($id==8)) ||
						(($type==9) && ($id==8)) ||
						(($type==10) && ($id==8)) ||
						(($type==11) && ($id==8)) ||
						(($type==12) && ($id==13))
					) {$itemname = 'Garuda '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase max stamine + 30<br />Double Damage rate + 5%<br />Energy + 15<br />Maximum HP +50<br />Increase skill attack rate + 25<br />Increase Wizardry Damage + 15%</font>";}
					elseif( /*Kantata Plate Set*/
						(($type==8) && ($id==9)) ||
						(($type==10) && ($id==9)) ||
						(($type==11) && ($id==9)) ||
						(($type==12) && ($id==23)) ||
						(($type==12) && ($id==9))
					) {$itemname = 'Kantata '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Energy + 15<br />Vitality + 30<br />Increase Wizardry Damage + 10%<br />Strength +15<br />Increase skill damage + 25<br />Excellent damage rate + 10%<br />Increase excellent damage + 20</font>";}
					elseif( /*Hyon Dragon Set*/
						(($type==7) && ($id==1)) ||
						(($type==10) && ($id==1)) ||
						(($type==11) && ($id==1)) ||
						(($type==0) && ($id==14))
					) {$itemname = 'Hyon '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase defense + 25<br />2x Dmg rate + 10%<br />Increase skill damage + 20<br />Critical damage rate + 15%<br />Excellent damage rate + 15%<br />Increase Critical Damage + 20<br />Increase Excellent Damage + 20</font>";}
					elseif( /*Apollo Pad Set*/
						(($type==7) && ($id==2)) ||
						(($type==8) && ($id==2)) ||
						(($type==9) && ($id==2)) ||
						(($type==10) && ($id==2)) ||
						(($type==5) && ($id==0)) ||
						(($type==12) && ($id==24)) ||
						(($type==12) && ($id==25))
					) {$itemname = 'Apollo '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Energy + 10<br />Incr. wizardry + 5%<br />Incr. attack. skill + 10<br />Maximum mana + 30<br />Maximum life + 30<br />Incr. max. AG + 20<br />Increase critical damage + 10<br />Increase excellent damage + 10<br />Energy + 30</font>";}
					elseif( /*Evis Bone Set*/
						(($type==8) && ($id==4)) ||
						(($type==9) && ($id==4)) ||
						(($type==11) && ($id==4)) ||
						(($type==12) && ($id==26))
					) {$itemname = 'Evis '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase attacking skill + 15<br />Increase stamina + 20<br />Increase wizardry damage + 10<br />Double damage rate 5%<br />Increase damage success rate +50<br />Increase AG regen rate +5</font>";}
					elseif( /*Hera Sphinx Set*/
						(($type==7) && ($id==7)) ||
						(($type==8) && ($id==7)) ||
						(($type==9) && ($id==7)) ||
						(($type==10) && ($id==7)) ||
						(($type==11) && ($id==7)) ||
						(($type==6) && ($id==6))
					) {$itemname = 'Hera '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Strength + 15<br />Increase wizardry dmg + 10%<br />Increase defensive skill when equipped with shield + 5%<br />Energy + 15<br />Increase attack success rate + 50<br />Critical damage rate + 10%<br />Excellent damage rate + 10%<br />Increase maximum life + 50<br />Increase maximum mana + 50</font>";}
					elseif( /*Anubis Legendary Set*/
						(($type==7) && ($id==3)) ||
						(($type==8) && ($id==3)) ||
						(($type==10) && ($id==3)) ||
						(($type==12) && ($id==21))
					) {$itemname = 'Anubis '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Double Dmg rate + 10%<br />Increase Max mana + 50<br />Increase. Wizardry dmg + 10%<br />Critical damage rate + 15%<br />Excellent damage rate + 15%<br />Increase critical damage + 20<br />Increase excellent damage + 20</font>";}
					elseif( /*Ceto Vine Set*/
						(($type==7) && ($id==10)) ||
						(($type==9) && ($id==10)) ||
						(($type==10) && ($id==10)) ||
						(($type==11) && ($id==10)) ||
						(($type==0) && ($id==2)) ||
						(($type==12) && ($id==22))
					) {$itemname = 'Ceto '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Agility + 10<br />Increase Max HP + 50<br />Increase Def skill + 20<br />Increase defensive skill while using shields + 5%<br />Increase Energy + 10<br />Increases Max HP + 50<br />Increase Strength + 20</font>";}
					elseif( /*Gaia Silk Set*/
						(($type==7) && ($id==11)) ||
						(($type==8) && ($id==11)) ||
						(($type==9) && ($id==11)) ||
						(($type==10) && ($id==11)) ||
						(($type==4) && ($id==9))
					) {$itemname = 'Gaia '.$itemname; 
					$ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font>
					<br />
					<br /><font color=gray>Increase attacking skill + 10
					<br />Increase max mana + 25<br />Power + 10
					<br />Double dmg rate + 5%
					<br />Agility + 30
					<br />Excellent damage rate + 10%
					<br />Increase excellent damage + 10 </font>";}
					elseif( /*Odin Winds Set*/
						(($type==7) && ($id==12)) ||
						(($type==8) && ($id==12)) ||
						(($type==9) && ($id==12)) ||
						(($type==10) && ($id==12)) ||
						(($type==11) && ($id==12))
					) {$itemname = 'Odin '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Energy + 15<br />Increase max life + 50<br />Increase attack success rate + 50<br />Agility + 30<br />Increase maximum mana + 50<br />Ignore enemy defensive skill + 5%<br />Increase maximum AG + 50</font>";}
								
					elseif( /*Argo Spirit Set*/
						(($type==8) && ($id==13)) ||
						(($type==9) && ($id==13)) ||
						(($type==10) && ($id==13))
					) {$itemname = 'Argo '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Agility + 30<br />Power + 30<br />Increase attacking skill + 25<br />Double damage rate + 5%</font>";}
					elseif( /*Gywen Guardian Set*/
						(($type==8) && ($id==14)) ||
						(($type==10) && ($id==14)) ||
						(($type==11) && ($id==14)) ||
						(($type==4) && ($id==5)) ||
						(($type==12) && ($id==28))
					) {$itemname = 'Gywen '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>2x dmg rate + 10%<br />Agility + 30<br />Incr min attacking skill + 20<br />Incr max attacking skill + 20<br />Critical damage rate + 15%<br />Excellent damage rate + 15%<br />Increase critical damage + 20<br />Increase excellent damage + 20</font>";}
					elseif( /*Gaion Storm Crow Set*/
						(($type==8) && ($id==15)) ||
						(($type==9) && ($id==15)) ||
						(($type==11) && ($id==15)) ||
						(($type==12) && ($id==27))
					) {$itemname = 'Gaion '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Ignore enemy defensive skill + 5%<br />2x damage rate + 15%<br />Inc. attacking skill + 15<br />Excellent damage rate + 15%<br />Increase excellent damage + 30<br />Increase wizardry + 20%<br />Increase Strength + 30</font>";}
					elseif( /*Agnis Adamantine Set*/
						(($type==7) && ($id==26)) ||
						(($type==8) && ($id==26)) ||
						(($type==9) && ($id==26)) ||
						(($type==12) && ($id==9))
					) {$itemname = 'Agnis '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Double Damage Rate + 10%<br />Increase Defense +40<br />Increase Skill Damage +20<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Increase Critical Damage +20<br />Increase Excellent Damage +20</font>";}
					elseif( /*Krono Red Wing Set*/
						(($type==7) && ($id==40)) ||
						(($type==9) && ($id==40)) ||
						(($type==10) && ($id==40)) ||
						(($type==12) && ($id==24))
					) {$itemname = 'Chrono '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Double Damage Rate +20%<br />Increase Defense +60<br />Increase Skill Damage +30<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Increase Critical Damage +20<br />Increase Excellent Damage +20</font>";}
					elseif( /*Vega's Sacred Set*/
						(($type==7) && ($id==59)) ||
						(($type==8) && ($id==59)) ||
						(($type==9) && ($id==59)) ||
						(($type==0) && ($id==32))
					) {$itemname = 'Vega '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase damage success rate +50<br />Increase stamina +50<br />Increase max. damage +5<br />Increase excellent damage rate 15%<br />Double damage rate 5%<br />Ignore enemies defensive skill 5%</font>";}
					else {$itemname = 'Ancient '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray></font>";}
					
					
					}
				break;
				case 6: case 10:
					{
					if( /*Obscure Leather Set*/
						(($type==7) && ($id==5)) ||
						(($type==9) && ($id==5)) ||
						(($type==11) && ($id==5)) ||
						(($type==6) && ($id==0))
					) {$itemname = 'Obscure '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase max. life +50<br />Increase agility +50<br />Increase defensive skill when using shield weapons 10%<br />Increase damage +30</font>";}
					elseif( /*Mist Bronze Set*/
						(($type==7) && ($id==0)) ||
						(($type==9) && ($id==0)) ||
						(($type==10) && ($id==0))
					) {$itemname = 'Mist '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase stamina +20<br />Increase skill attacking rate +30<br />Double damage rate 10%<br />Increase agility +20</font>";}
					elseif( /*Berserker Scale Set*/
						(($type==7) && ($id==6)) ||
						(($type==8) && ($id==6)) ||
						(($type==9) && ($id==6)) ||
						(($type==10) && ($id==6)) ||
						(($type==11) && ($id==6))
					) {$itemname = 'Berserker '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase max damage +10<br />Increase max damage +20<br />Increase max damage +30<br />Increase max damage +40<br />Increase skill attacking rate +40<br />Increase strength +40</font>";}
					elseif( /*Cloud Brass Set*/
						(($type==7) && ($id==8)) ||
						(($type==9) && ($id==8)) 
					) {$itemname = 'Cloud '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase critical dmage rate 20%<br />Increase critical damage + 50</font>";}
					elseif( /*Rave Plate Set*/
						(($type==7) && ($id==9)) ||
						(($type==8) && ($id==9)) ||
						(($type==9) && ($id==9)) 
					) {$itemname = 'Rave '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase skill atacking rate + 20<br />Double damage rate 10%<br />Increase damage when using two handed weapon + 30%<br />Ignore enemies defensive skill 5%</font>";}
					elseif( /*Vicious Dragon Set*/
						(($type==7) && ($id==1)) ||
						(($type==8) && ($id==1)) ||
						(($type==9) && ($id==1)) ||
						(($type==12) && ($id==22))
					) {$itemname = 'Vicious '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Skill Damage +15<br />Increased Damage +15<br />Double Damage Rate + 10%<br />Increase Minimum Attack Damage +20<br />Increase Maximum Attack Damage +30<br />Ignore Opponents Defensive Skill + 5%</font>";}
					elseif( /*Banek Pad Set*/
						(($type==7) && ($id==2)) ||
						(($type==9) && ($id==2)) ||
						(($type==11) && ($id==2))
					) {$itemname = 'Banek '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Wizardy Dmg +10%<br />Increase energy +20<br />Increase skill attacking rate +30<br />Increase max. mana +100</font>";}
					elseif( /*Sylion Bone Set*/
						(($type==7) && ($id==4)) ||
						(($type==8) && ($id==4)) ||
						(($type==10) && ($id==4)) ||
						(($type==11) && ($id==4))
					) {$itemname = 'Sylion '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Double damage rate 5%<br />Increase critical damage 5%<br />Increase defensive skill +20<br />Increase strength +50<br />Increase agility +50<br />Increase stamina +50<br />Increase energy +50</font>";}
					elseif( /*Myne Sphinx Set*/
						(($type==8) && ($id==7)) ||
						(($type==9) && ($id==7)) ||
						(($type==11) && ($id==7))
					) {$itemname = 'Myne '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase energy +30<br />Increase defensive skill +30<br />Increase max. mana +100<br />Increase skill atacking rate +15</font>";}
					elseif( /*Isis Legendary Set*/
						(($type==7) && ($id==3)) ||
						(($type==8) && ($id==3)) ||
						(($type==9) && ($id==3)) ||
						(($type==11) && ($id==3))
					) {$itemname = 'Isis '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Skill Damage +10<br />Double Damage Rate + 10%<br />Energy +30<br />Increase Wizardry Damage + 10%<br />Ignore Opponents Defensive Skill + 5%</font>";}
					elseif( /*Drak Vine Set*/
						(($type==7) && ($id==10)) ||
						(($type==8) && ($id==10)) ||
						(($type==9) && ($id==10)) ||
						(($type==11) && ($id==10))
					) {$itemname = 'Drak '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase agility +20<br />Increase damage +25<br />Double damage rate 20%<br />Increase defensive skill +40<br />Increase critical damage rate 10%</font>";}
					elseif( /*Peize Silk Set*/
						(($type==9) && ($id==11)) ||
						(($type==10) && ($id==11)) ||
						(($type==11) && ($id==11))
					) {$itemname = 'Peize '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase max. life +100<br />Increase max. mana +100<br />Increase defensive skill +100</font>";}
					elseif( /*Elvian Wind Set*/
						(($type==9) && ($id==12)) ||
						(($type==11) && ($id==12))
					) {$itemname = 'Elvian '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase agility +30<br />Ignore enemies defensive skill 5%</font>";}
					elseif( /*Karis Spirit Set*/
						(($type==7) && ($id==13)) ||
						(($type==9) && ($id==13)) ||
						(($type==11) && ($id==13))
					) {$itemname = 'Karis '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase skill attacking rate +15<br />Double damage rate 10%<br />Increase critical damage rate 10%<br />Increase agility +40</font>";}
					elseif( /*Aruane Guardian Set*/
						(($type==7) && ($id==14)) ||
						(($type==8) && ($id==14)) ||
						(($type==9) && ($id==14)) ||
						(($type==11) && ($id==14))
					) {$itemname = 'Aruane '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increased Damage +10<br />Double Damage Rate + 10%<br />Increase Skill Damage +20<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Ignore Opponents Defensive Skill + 5%</font>";}
					elseif( /*Muren Storm Crow Set*/
						(($type==8) && ($id==15)) ||
						(($type==9) && ($id==15)) ||
						(($type==10) && ($id==15)) ||
						(($type==12) && ($id==21))
					) {$itemname = 'Muren '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Skill Damage +10<br />Increase Wizardry Damage + 10%<br />Double Damage Rate + 10%<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Increase Defense +25<br />Increase Two-handed Weapon Equipment Damage + 20%</font>";}
					elseif( /*Browii Adamantine Set*/
						(($type==9) && ($id==26)) ||
						(($type==10) && ($id==26)) ||
						(($type==11) && ($id==26)) ||
						(($type==12) && ($id==25))
					) {$itemname = 'Browii '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Damage +20<br />Increase Skill Damage +20<br />Increase Energy +30<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Ignore Opponents Defensive Skill + 5%<br />Increase Command +30</font>";}
					elseif( /*Semeden Red Wing Set*/
						(($type==7) && ($id==40)) ||
						(($type==8) && ($id==40)) ||
						(($type==10) && ($id==40)) ||
						(($type==11) && ($id==40))
					) {$itemname = 'Semeden '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Wizardry + 15%<br />Increase Skill Damage +25<br />Increase Energy +30<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Ignore Opponents Defensive Skill + 5%</font>";}
					elseif( /*Chamereuui's Sacred Set*/
						(($type==8) && ($id==59)) ||
						(($type==9) && ($id==59)) ||
						(($type==0) && ($id==32)) ||
						(($type==11) && ($id==59))
					) {$itemname = 'Chamereuui '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase defensiva skill +50<br />Double damage rate 5%<br />Increase damage +30<br />Increase excellent damage rate 15%<br />Increase skill attacking rate +30<br />Increase excellent damage +20</font>";}
					else {$itemname = 'Ancient '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray></font>";}
					
					}
				break;
				}
				if($level > 0) { $level = " + ".$item->getLevel(); } else { $level = ""; }
				if($exe > 0){ 
					$class = "title";
					if($item->getGroup() > 6) {
						if ($exe >= 32) { $exe -= 32; $exe_new .= "Increase MaxHP +4% <br />"; }
						if ($exe >= 16) { $exe -= 16; $exe_new .= "Increase MaxMana +4% <br />"; }
						if ($exe >= 8) { $exe -= 8; $exe_new .= "Damage Decrease +4% <br />";  }
						if ($exe >= 4) { $exe -= 4; $exe_new .= "Reflect damage +5% <br />";  }
						if ($exe >= 2) { $exe -= 2; $exe_new .= "Defense success rate +10% <br />"; }
						if ($exe >= 1) { $exe -= 1; $exe_new .= "Increase Zen After Hunt +40% <br />";  }
					}elseif($item->getGroup() > 11){
						if ($exe >= 32) { $exe -= 32; $exe_new .= ""; }
						if ($exe >= 16) { $exe -= 16; $exe_new .= "Increase Attacking Speed +5 <br />"; }
						if ($exe >= 8) { $exe -= 8; $exe_new .= "Increase Stamina +50 <br />";  }
						if ($exe >= 4) { $exe -= 4; $exe_new .= "Ignores 3% of your opponent's armor <br />";  }
						if ($exe >= 2) { $exe -= 2; $exe_new .= "Increased Mana + ".(50+(5*$level))." <br />"; }
						if ($exe >= 1) { $exe -= 1; $exe_new .= "Increased Health + ".(50+(5*$level))." <br />";  }
					}else {
						if ($exe >= 32) { $exe -= 32; $exe_new .= "Excellent Damage Rate + 10% <br />"; }
						if ($exe >= 16) { $exe -= 16; $exe_new .= "Increase Damage + 2% <br />"; }
						if ($exe >= 8) { $exe -= 8; $exe_new .= "Increase Speed + 7 <br />";  }
						if ($exe >= 4) { $exe -= 4; $exe_new .= "Increase Damage + level/20 <br />";  }
						if ($exe >= 2) { $exe -= 2; $exe_new .= "Increase life after monster + life/8 <br />"; }
						if ($exe >= 1) { $exe -= 1; $exe_new .= "Increase mana after monster + mana/8 <br />";  }
					}
				} 
				else { $class= ""; $exe_new = ""; }
				if($skill > 0) { $skill = " + Skill <br />"; } else { $skill = ""; }
				if($luck > 0) { $luck = "Luck (success rate of Jewel of Soul +25%) <br />Luck (critical damage rate +5%) <br />"; } else { $luck = ""; }
				if($opt > 0) {$opt_new = "+ Option ".$opt * 4 ." <br />"; } else {$opt_new = '';}
				echo "<form action = '' method = 'post'>";
				echo "
				<tr>
				<td>
				<p class='$class'>
				".$itemname." ".$level."
				</p>
				<span class= 'opt'>
				$luck
				$skill
				$opt_new
				</span>
				<p class = 'title'>$exe_new</p>
				<p class = 'title'>$ac_new</p>
				</td>
				<td> 
				<input type = 'hidden' name = 'seller' value = '".$_SESSION['user']."'/>
				<input type = 'hidden' name = 'item_hex' value = '".$new_item[$i]."'/>
				Bid Price <br /><input type = 'number' name = 'bidPrice' palaceholder = 'Bid price' style = 'width:100px;' class = 'input' max = '650000' min = '1' required/>
				<br />
				Buyout Price <br /><input type = 'number' name = 'buyPrice' palaceholder = 'Buyout price' style = 'width:100px;' class = 'input' max = '650000' min = '1' required/>
				</td>
				<td ><input type = 'submit' name = 'submit' value = 'Create Auction' class = 'ok'/></td>
				</tr>
				";
				echo "</form>";
			}
		}
		echo "</table>";
	}
	
	function postItem($item,$seller,$price1,$price2){
		$db = $this->webConn;
		$db2 = $this->gameConn;
		$web_sql = "INSERT INTO market (item_hex, seller, price1, price2) 
		VALUES ('".$item."', '".$seller."', '".$price1."', ".$price2.");";
		
		$sql = "SELECT * FROM warehouse WHERE AccountID = '".$seller."'";
		$stmt = sqlsrv_query($db2, $sql)or die(print_r(sqlsrv_errors(), true));
		$fetch = sqlsrv_fetch_array($stmt);
		$items = strtoupper(bin2hex($fetch['Items']));
		

		$new_item_query = preg_replace("/".$item."/","FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",$items);
		
		$test = '0x'.$new_item_query;
		
		//echo $test;
		
		sqlsrv_query($db2,"UPDATE warehouse SET Items = $test WHERE AccountID = '".$seller."'")or die(print_r(sqlsrv_errors(), true));
		sqlsrv_query($db,$web_sql)or die(print_r(sqlsrv_errors(), true));
		
		$this->errorHandle("You posted item : ".$this->getItemName($item));
	}
	
	function cancelItem($item){
		$db = $this->webConn;
		$sql = "SELECT * FROM market WHERE item_hex = '".$item."'";
		$stmt = sqlsrv_query($db,$sql);
		$fetch = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC);
		
		$seller = $fetch['seller'];
		
		sqlsrv_query($db,"INSERT INTO online_warehouse (AccountID,item_hex) VALUES ('".$seller."','".$item."')")or die(print_r(sqlsrv_errors(), true));
		
		sqlsrv_query($db,"DELETE FROM market WHERE item_hex = '".$item."'")or die(print_r(sqlsrv_errors(), true));
		
		$this->errorHandle("Auction canceled : ".$this->getItemName($item));
	}
	
	function onlineBank($user){
		
		$db = $this->webConn;
		$sql = "SELECT * FROM online_warehouse WHERE AccountID = '".$user."'";
		$stmt = sqlsrv_query($db, $sql)or die(print_r(sqlsrv_errors(), true));
		$i = 1;
			while($key = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
				$item = new Items($key['item_hex']);
				$level = $item->getLevel();
				$type = $item->getGroup();
				$id = $item->getId();
				$exe = $item->getExcellent();
				$skill = $item->getSkill();
				$luck = $item->getLuck();
				$opt = $item->getOption();
				$exe_new = "";
				$ac = $item->getAncient();
				$ac_new = "";
				$itemname = $this->getItemName($key['item_hex']);
				switch($ac)
				{
				case 5: case 9:
					{
					if( /*Warrior Leather Set*/
						(($type==7) && ($id==5)) ||          
						(($type==8) && ($id==5)) ||
						(($type==9) && ($id==5)) ||
						(($type==10) && ($id==5)) ||
						(($type==11) && ($id==5)) ||
						(($type==12) && ($id==8)) ||
						(($type==2) && ($id==1))
					) {$itemname = 'Warrior '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Strength +10<br />Incr. attack rate +10<br />Incr. maximum AG +20<br />AG increase Rate + 5<br />Increase Defense + 20<br />Increase agility +10<br />Critical damage rate + 5%<br />Excellent damage rate + 5%<br />Increase strength + 25</font>";}
					elseif( /*Hyperion Bronze Set*/
						(($type==8) && ($id==0)) ||
						(($type==9) && ($id==0)) ||
						(($type==11) && ($id==0)) 
					) {$itemname = 'Hyperion '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Energy + 15<br />Increase Agility + 15<br />Increase skill attacking rate +20<br />Increase Mana + 30</font>";}
					elseif( /*Eplete Scale Set*/
						(($type==7) && ($id==6)) ||
						(($type==8) && ($id==6)) ||
						(($type==9) && ($id==6)) ||
						(($type==6) && ($id==9)) ||
						(($type==12) && ($id==2)) 
					) {$itemname = 'Eplete '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase atack skill + 15<br />Increase atack + 50<br />Increase Wizardry dmg + 5%<br />Maximum HP +50<br />Increase maximum AG +30<br />Incr. Critical damage rate +10%<br />Excellent damage rate +10</font>";}
					elseif( /*Garuda Brass Set*/
						(($type==8) && ($id==8)) ||
						(($type==9) && ($id==8)) ||
						(($type==10) && ($id==8)) ||
						(($type==11) && ($id==8)) ||
						(($type==12) && ($id==13))
					) {$itemname = 'Garuda '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase max stamine + 30<br />Double Damage rate + 5%<br />Energy + 15<br />Maximum HP +50<br />Increase skill attack rate + 25<br />Increase Wizardry Damage + 15%</font>";}
					elseif( /*Kantata Plate Set*/
						(($type==8) && ($id==9)) ||
						(($type==10) && ($id==9)) ||
						(($type==11) && ($id==9)) ||
						(($type==12) && ($id==23)) ||
						(($type==12) && ($id==9))
					) {$itemname = 'Kantata '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Energy + 15<br />Vitality + 30<br />Increase Wizardry Damage + 10%<br />Strength +15<br />Increase skill damage + 25<br />Excellent damage rate + 10%<br />Increase excellent damage + 20</font>";}
					elseif( /*Hyon Dragon Set*/
						(($type==7) && ($id==1)) ||
						(($type==10) && ($id==1)) ||
						(($type==11) && ($id==1)) ||
						(($type==0) && ($id==14))
					) {$itemname = 'Hyon '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase defense + 25<br />2x Dmg rate + 10%<br />Increase skill damage + 20<br />Critical damage rate + 15%<br />Excellent damage rate + 15%<br />Increase Critical Damage + 20<br />Increase Excellent Damage + 20</font>";}
					elseif( /*Apollo Pad Set*/
						(($type==7) && ($id==2)) ||
						(($type==8) && ($id==2)) ||
						(($type==9) && ($id==2)) ||
						(($type==10) && ($id==2)) ||
						(($type==5) && ($id==0)) ||
						(($type==12) && ($id==24)) ||
						(($type==12) && ($id==25))
					) {$itemname = 'Apollo '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Energy + 10<br />Incr. wizardry + 5%<br />Incr. attack. skill + 10<br />Maximum mana + 30<br />Maximum life + 30<br />Incr. max. AG + 20<br />Increase critical damage + 10<br />Increase excellent damage + 10<br />Energy + 30</font>";}
					elseif( /*Evis Bone Set*/
						(($type==8) && ($id==4)) ||
						(($type==9) && ($id==4)) ||
						(($type==11) && ($id==4)) ||
						(($type==12) && ($id==26))
					) {$itemname = 'Evis '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase attacking skill + 15<br />Increase stamina + 20<br />Increase wizardry damage + 10<br />Double damage rate 5%<br />Increase damage success rate +50<br />Increase AG regen rate +5</font>";}
					elseif( /*Hera Sphinx Set*/
						(($type==7) && ($id==7)) ||
						(($type==8) && ($id==7)) ||
						(($type==9) && ($id==7)) ||
						(($type==10) && ($id==7)) ||
						(($type==11) && ($id==7)) ||
						(($type==6) && ($id==6))
					) {$itemname = 'Hera '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Strength + 15<br />Increase wizardry dmg + 10%<br />Increase defensive skill when equipped with shield + 5%<br />Energy + 15<br />Increase attack success rate + 50<br />Critical damage rate + 10%<br />Excellent damage rate + 10%<br />Increase maximum life + 50<br />Increase maximum mana + 50</font>";}
					elseif( /*Anubis Legendary Set*/
						(($type==7) && ($id==3)) ||
						(($type==8) && ($id==3)) ||
						(($type==10) && ($id==3)) ||
						(($type==12) && ($id==21))
					) {$itemname = 'Anubis '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Double Dmg rate + 10%<br />Increase Max mana + 50<br />Increase. Wizardry dmg + 10%<br />Critical damage rate + 15%<br />Excellent damage rate + 15%<br />Increase critical damage + 20<br />Increase excellent damage + 20</font>";}
					elseif( /*Ceto Vine Set*/
						(($type==7) && ($id==10)) ||
						(($type==9) && ($id==10)) ||
						(($type==10) && ($id==10)) ||
						(($type==11) && ($id==10)) ||
						(($type==0) && ($id==2)) ||
						(($type==12) && ($id==22))
					) {$itemname = 'Ceto '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Agility + 10<br />Increase Max HP + 50<br />Increase Def skill + 20<br />Increase defensive skill while using shields + 5%<br />Increase Energy + 10<br />Increases Max HP + 50<br />Increase Strength + 20</font>";}
					elseif( /*Gaia Silk Set*/
						(($type==7) && ($id==11)) ||
						(($type==8) && ($id==11)) ||
						(($type==9) && ($id==11)) ||
						(($type==10) && ($id==11)) ||
						(($type==4) && ($id==9))
					) {$itemname = 'Gaia '.$itemname; 
					$ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font>
					<br />
					<br /><font color=gray>Increase attacking skill + 10
					<br />Increase max mana + 25<br />Power + 10
					<br />Double dmg rate + 5%
					<br />Agility + 30
					<br />Excellent damage rate + 10%
					<br />Increase excellent damage + 10 </font>";}
					elseif( /*Odin Winds Set*/
						(($type==7) && ($id==12)) ||
						(($type==8) && ($id==12)) ||
						(($type==9) && ($id==12)) ||
						(($type==10) && ($id==12)) ||
						(($type==11) && ($id==12))
					) {$itemname = 'Odin '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Energy + 15<br />Increase max life + 50<br />Increase attack success rate + 50<br />Agility + 30<br />Increase maximum mana + 50<br />Ignore enemy defensive skill + 5%<br />Increase maximum AG + 50</font>";}
								
					elseif( /*Argo Spirit Set*/
						(($type==8) && ($id==13)) ||
						(($type==9) && ($id==13)) ||
						(($type==10) && ($id==13))
					) {$itemname = 'Argo '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Agility + 30<br />Power + 30<br />Increase attacking skill + 25<br />Double damage rate + 5%</font>";}
					elseif( /*Gywen Guardian Set*/
						(($type==8) && ($id==14)) ||
						(($type==10) && ($id==14)) ||
						(($type==11) && ($id==14)) ||
						(($type==4) && ($id==5)) ||
						(($type==12) && ($id==28))
					) {$itemname = 'Gywen '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>2x dmg rate + 10%<br />Agility + 30<br />Incr min attacking skill + 20<br />Incr max attacking skill + 20<br />Critical damage rate + 15%<br />Excellent damage rate + 15%<br />Increase critical damage + 20<br />Increase excellent damage + 20</font>";}
					elseif( /*Gaion Storm Crow Set*/
						(($type==8) && ($id==15)) ||
						(($type==9) && ($id==15)) ||
						(($type==11) && ($id==15)) ||
						(($type==12) && ($id==27))
					) {$itemname = 'Gaion '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Ignore enemy defensive skill + 5%<br />2x damage rate + 15%<br />Inc. attacking skill + 15<br />Excellent damage rate + 15%<br />Increase excellent damage + 30<br />Increase wizardry + 20%<br />Increase Strength + 30</font>";}
					elseif( /*Agnis Adamantine Set*/
						(($type==7) && ($id==26)) ||
						(($type==8) && ($id==26)) ||
						(($type==9) && ($id==26)) ||
						(($type==12) && ($id==9))
					) {$itemname = 'Agnis '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Double Damage Rate + 10%<br />Increase Defense +40<br />Increase Skill Damage +20<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Increase Critical Damage +20<br />Increase Excellent Damage +20</font>";}
					elseif( /*Krono Red Wing Set*/
						(($type==7) && ($id==40)) ||
						(($type==9) && ($id==40)) ||
						(($type==10) && ($id==40)) ||
						(($type==12) && ($id==24))
					) {$itemname = 'Chrono '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Double Damage Rate +20%<br />Increase Defense +60<br />Increase Skill Damage +30<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Increase Critical Damage +20<br />Increase Excellent Damage +20</font>";}
					elseif( /*Vega's Sacred Set*/
						(($type==7) && ($id==59)) ||
						(($type==8) && ($id==59)) ||
						(($type==9) && ($id==59)) ||
						(($type==0) && ($id==32))
					) {$itemname = 'Vega '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase damage success rate +50<br />Increase stamina +50<br />Increase max. damage +5<br />Increase excellent damage rate 15%<br />Double damage rate 5%<br />Ignore enemies defensive skill 5%</font>";}
					else {$itemname = 'Ancient '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray></font>";}
					
					
					}
				break;
				case 6: case 10:
					{
					if( /*Obscure Leather Set*/
						(($type==7) && ($id==5)) ||
						(($type==9) && ($id==5)) ||
						(($type==11) && ($id==5)) ||
						(($type==6) && ($id==0))
					) {$itemname = 'Obscure '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase max. life +50<br />Increase agility +50<br />Increase defensive skill when using shield weapons 10%<br />Increase damage +30</font>";}
					elseif( /*Mist Bronze Set*/
						(($type==7) && ($id==0)) ||
						(($type==9) && ($id==0)) ||
						(($type==10) && ($id==0))
					) {$itemname = 'Mist '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase stamina +20<br />Increase skill attacking rate +30<br />Double damage rate 10%<br />Increase agility +20</font>";}
					elseif( /*Berserker Scale Set*/
						(($type==7) && ($id==6)) ||
						(($type==8) && ($id==6)) ||
						(($type==9) && ($id==6)) ||
						(($type==10) && ($id==6)) ||
						(($type==11) && ($id==6))
					) {$itemname = 'Berserker '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase max damage +10<br />Increase max damage +20<br />Increase max damage +30<br />Increase max damage +40<br />Increase skill attacking rate +40<br />Increase strength +40</font>";}
					elseif( /*Cloud Brass Set*/
						(($type==7) && ($id==8)) ||
						(($type==9) && ($id==8)) 
					) {$itemname = 'Cloud '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase critical dmage rate 20%<br />Increase critical damage + 50</font>";}
					elseif( /*Rave Plate Set*/
						(($type==7) && ($id==9)) ||
						(($type==8) && ($id==9)) ||
						(($type==9) && ($id==9)) 
					) {$itemname = 'Rave '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase skill atacking rate + 20<br />Double damage rate 10%<br />Increase damage when using two handed weapon + 30%<br />Ignore enemies defensive skill 5%</font>";}
					elseif( /*Vicious Dragon Set*/
						(($type==7) && ($id==1)) ||
						(($type==8) && ($id==1)) ||
						(($type==9) && ($id==1)) ||
						(($type==12) && ($id==22))
					) {$itemname = 'Vicious '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Skill Damage +15<br />Increased Damage +15<br />Double Damage Rate + 10%<br />Increase Minimum Attack Damage +20<br />Increase Maximum Attack Damage +30<br />Ignore Opponents Defensive Skill + 5%</font>";}
					elseif( /*Banek Pad Set*/
						(($type==7) && ($id==2)) ||
						(($type==9) && ($id==2)) ||
						(($type==11) && ($id==2))
					) {$itemname = 'Banek '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Wizardy Dmg +10%<br />Increase energy +20<br />Increase skill attacking rate +30<br />Increase max. mana +100</font>";}
					elseif( /*Sylion Bone Set*/
						(($type==7) && ($id==4)) ||
						(($type==8) && ($id==4)) ||
						(($type==10) && ($id==4)) ||
						(($type==11) && ($id==4))
					) {$itemname = 'Sylion '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Double damage rate 5%<br />Increase critical damage 5%<br />Increase defensive skill +20<br />Increase strength +50<br />Increase agility +50<br />Increase stamina +50<br />Increase energy +50</font>";}
					elseif( /*Myne Sphinx Set*/
						(($type==8) && ($id==7)) ||
						(($type==9) && ($id==7)) ||
						(($type==11) && ($id==7))
					) {$itemname = 'Myne '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase energy +30<br />Increase defensive skill +30<br />Increase max. mana +100<br />Increase skill atacking rate +15</font>";}
					elseif( /*Isis Legendary Set*/
						(($type==7) && ($id==3)) ||
						(($type==8) && ($id==3)) ||
						(($type==9) && ($id==3)) ||
						(($type==11) && ($id==3))
					) {$itemname = 'Isis '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Skill Damage +10<br />Double Damage Rate + 10%<br />Energy +30<br />Increase Wizardry Damage + 10%<br />Ignore Opponents Defensive Skill + 5%</font>";}
					elseif( /*Drak Vine Set*/
						(($type==7) && ($id==10)) ||
						(($type==8) && ($id==10)) ||
						(($type==9) && ($id==10)) ||
						(($type==11) && ($id==10))
					) {$itemname = 'Drak '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase agility +20<br />Increase damage +25<br />Double damage rate 20%<br />Increase defensive skill +40<br />Increase critical damage rate 10%</font>";}
					elseif( /*Peize Silk Set*/
						(($type==9) && ($id==11)) ||
						(($type==10) && ($id==11)) ||
						(($type==11) && ($id==11))
					) {$itemname = 'Peize '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase max. life +100<br />Increase max. mana +100<br />Increase defensive skill +100</font>";}
					elseif( /*Elvian Wind Set*/
						(($type==9) && ($id==12)) ||
						(($type==11) && ($id==12))
					) {$itemname = 'Elvian '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase agility +30<br />Ignore enemies defensive skill 5%</font>";}
					elseif( /*Karis Spirit Set*/
						(($type==7) && ($id==13)) ||
						(($type==9) && ($id==13)) ||
						(($type==11) && ($id==13))
					) {$itemname = 'Karis '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase skill attacking rate +15<br />Double damage rate 10%<br />Increase critical damage rate 10%<br />Increase agility +40</font>";}
					elseif( /*Aruane Guardian Set*/
						(($type==7) && ($id==14)) ||
						(($type==8) && ($id==14)) ||
						(($type==9) && ($id==14)) ||
						(($type==11) && ($id==14))
					) {$itemname = 'Aruane '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increased Damage +10<br />Double Damage Rate + 10%<br />Increase Skill Damage +20<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Ignore Opponents Defensive Skill + 5%</font>";}
					elseif( /*Muren Storm Crow Set*/
						(($type==8) && ($id==15)) ||
						(($type==9) && ($id==15)) ||
						(($type==10) && ($id==15)) ||
						(($type==12) && ($id==21))
					) {$itemname = 'Muren '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Skill Damage +10<br />Increase Wizardry Damage + 10%<br />Double Damage Rate + 10%<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Increase Defense +25<br />Increase Two-handed Weapon Equipment Damage + 20%</font>";}
					elseif( /*Browii Adamantine Set*/
						(($type==9) && ($id==26)) ||
						(($type==10) && ($id==26)) ||
						(($type==11) && ($id==26)) ||
						(($type==12) && ($id==25))
					) {$itemname = 'Browii '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Damage +20<br />Increase Skill Damage +20<br />Increase Energy +30<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Ignore Opponents Defensive Skill + 5%<br />Increase Command +30</font>";}
					elseif( /*Semeden Red Wing Set*/
						(($type==7) && ($id==40)) ||
						(($type==8) && ($id==40)) ||
						(($type==10) && ($id==40)) ||
						(($type==11) && ($id==40))
					) {$itemname = 'Semeden '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Wizardry + 15%<br />Increase Skill Damage +25<br />Increase Energy +30<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Ignore Opponents Defensive Skill + 5%</font>";}
					elseif( /*Chamereuui's Sacred Set*/
						(($type==8) && ($id==59)) ||
						(($type==9) && ($id==59)) ||
						(($type==0) && ($id==32)) ||
						(($type==11) && ($id==59))
					) {$itemname = 'Chamereuui '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase defensiva skill +50<br />Double damage rate 5%<br />Increase damage +30<br />Increase excellent damage rate 15%<br />Increase skill attacking rate +30<br />Increase excellent damage +20</font>";}
					else {$itemname = 'Ancient '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray></font>";}
					
					}
				break;
				}
				if($level > 0) { $level = " + ".$item->getLevel(); } else { $level = ""; }
				if($exe > 0){ 
					$class = "title";
					if($item->getGroup() > 6) {
						if ($exe >= 32) { $exe -= 32; $exe_new .= "Increase MaxHP +4% <br />"; }
						if ($exe >= 16) { $exe -= 16; $exe_new .= "Increase MaxMana +4% <br />"; }
						if ($exe >= 8) { $exe -= 8; $exe_new .= "Damage Decrease +4% <br />";  }
						if ($exe >= 4) { $exe -= 4; $exe_new .= "Reflect damage +5% <br />";  }
						if ($exe >= 2) { $exe -= 2; $exe_new .= "Defense success rate +10% <br />"; }
						if ($exe >= 1) { $exe -= 1; $exe_new .= "Increase Zen After Hunt +40% <br />";  }
					}elseif($item->getGroup() > 11){
						if ($exe >= 32) { $exe -= 32; $exe_new .= ""; }
						if ($exe >= 16) { $exe -= 16; $exe_new .= "Increase Attacking Speed +5 <br />"; }
						if ($exe >= 8) { $exe -= 8; $exe_new .= "Increase Stamina +50 <br />";  }
						if ($exe >= 4) { $exe -= 4; $exe_new .= "Ignores 3% of your opponent's armor <br />";  }
						if ($exe >= 2) { $exe -= 2; $exe_new .= "Increased Mana + ".(50+(5*$level))." <br />"; }
						if ($exe >= 1) { $exe -= 1; $exe_new .= "Increased Health + ".(50+(5*$level))." <br />";  }
					}else {
						if ($exe >= 32) { $exe -= 32; $exe_new .= "Excellent Damage Rate + 10% <br />"; }
						if ($exe >= 16) { $exe -= 16; $exe_new .= "Increase Damage + 2% <br />"; }
						if ($exe >= 8) { $exe -= 8; $exe_new .= "Increase Speed + 7 <br />";  }
						if ($exe >= 4) { $exe -= 4; $exe_new .= "Increase Damage + level/20 <br />";  }
						if ($exe >= 2) { $exe -= 2; $exe_new .= "Increase life after monster + life/8 <br />"; }
						if ($exe >= 1) { $exe -= 1; $exe_new .= "Increase mana after monster + mana/8 <br />";  }
					}
				} 
				else { $class= ""; $exe_new = ""; }
				if($skill > 0) { $skill = " + Skill <br />"; } else { $skill = ""; }
				if($luck > 0) { $luck = "Luck (success rate of Jewel of Soul +25%) <br />Luck (critical damage rate +5%) <br />"; } else { $luck = ""; }
				if($opt > 0) {$opt_new = "+ Option ".$opt * 4 ." <br />"; } else {$opt_new = '';}
				echo "<form action = '' method = 'post'>";
				echo "
				<tr>
				<td>
				<p class='$class'>
				".$itemname." ".$level."
				</p>
				<span class= 'opt'>
				$luck
				$skill
				$opt_new
				</span>
				<p class = 'title'>$exe_new</p>
				<p class = 'title'>$ac_new</p>
				</td>";
					echo "<td>";
					echo "<input type = 'hidden' name = 'item' value = '".$key['item_hex']."'>";
					echo "<input type = 'hidden' name = 'user' value = '".$key['AccountID']."'>";
						echo "<p><input type = 'submit' name = 'submit' value = 'Send In Game' class ='ok'/></p>";
					echo "</td>";
				echo "</tr>";
				echo "</form>";
			}
			
	}
	
	
	function sendItemInGame($item,$user){
		$db = $this->webConn;
		$db2 = $this->gameConn;
		
		$sql = "SELECT * FROM warehouse WHERE AccountID = '".$user."'";
		$stmt = sqlsrv_query($db2, $sql)or die(print_r(sqlsrv_errors(), true));
		$fetch = sqlsrv_fetch_array($stmt);
		$items = strtoupper(bin2hex($fetch['Items']));
		$split = str_split($items,32);
		
		$i = 119;
		
		while($i++ <=  239){
			
			if($split[$i] != "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"){
				
				header("Location: http://www.miraclemu.eu/webshop.php?action=market&p=bank");
				echo "<span class = 'red'>Empty extended warehous it's used only for market </span>";
				exit();
				
			}
			
			if($split[$i] == "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF") {
				$split[$i] = $item;
				Break;
			}
			
		}
		
		$new = implode("",$split);
		
			//$new_item_query = str_replace("/FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF/",$item,$items);
			$test = '0x'.$new;
			echo $test;
			sqlsrv_query($db2,"UPDATE warehouse SET Items = $test WHERE AccountID = '".$user."'")or die(print_r(sqlsrv_errors(), true));
			sqlsrv_query($db,"DELETE FROM online_warehouse WHERE item_hex = '".$item."'")or die(print_r(sqlsrv_errors(), true));
			$this->errorHandle("You placed item : ".$this->getItemName($item)." In your bank");

		
	}
	
	function getItemName($hex){
		$id = hexdec(substr($hex, 0, 2));
		$group = hexdec(substr($hex, 18, 1));

		
		$item = array(
			0 => array(
				'0' => 'Kriss',
				'1' => 'Short Sword',
				'2' => 'Rapier',
				'3' => 'Katana',
				'4' => 'Sword of Assassin',
				'5' => 'Blade',
				'6' => 'Gladius',
				'7' => 'Falchio',
				'8' => 'Serpent Sword',
				'9' => 'Sword of Salamander',
				'10' => 'Light Saber',
				'11' => 'Legendary Sword',
				'12' => 'Heliacal Sword',
				'13' => 'Double Blade',
				'14' => 'Lighting Sword',
				'15' => 'Giant Sword',
				'16' => 'Sword of Destruction',
				'17' => 'Dark Breaker',
				'18' => 'Thunder Blade',
				'19' => 'Divine Sword of Arcangel',
				'20' => 'Knight Blade',
				'21' => 'Dark Reign Blade',
				'22' => 'Bone Blade',
				'23' => 'Explosion Blade"',
				'24' => 'Daybreak',
				'25' => 'Sword Dancer',
				'26' => 'Flameberge',
				'27' => 'Sword Breaker',
				'28' => 'Rune Bastard Sword',
				'29' => 'Sonic Blade',
				'30' => 'Asura',
				'31' => 'Rune Blade',
				'32' => 'Sacred Glove',
				'33' => 'Holy Storm Claw',
				'34' => 'Piercing Blade Glove',
				'35' => 'Phoenix Soul Star',
				'36' => 'Cyclone Sword',
				'37' => 'Blast Break',
				'39' => 'Lighting Sword-J',
				'40' => 'Sacred Glove-J',
				'41' => 'Pandora Pick',
			),
			1=>array(
				'0' => 'Small Axe',
				'1' => 'Hand Axe',
				'2' => 'Double Axe',
				'3' => 'Tomhawk',
				'4' => 'Elven Axe',
				'5' => 'Battle Axe',
				'6' => 'Nikea Axe',
				'7' => 'larkan Axe',
				'8' => 'Creset Axe',
			),
			2=>array(
				'0' => 'Mace',
				'1' => 'Morning Star',
				'2' => 'Flail',
				'3' => 'Great Hammer',
				'4' => 'Crystal Morning Star',
				'5' => 'Crystal Sword',
				'6' => 'Chaos Dragon Axe',
				'7' => 'Elemental mace',
				'8' => 'Battle Scepter',
				'9' => 'Master Scepter',
				'10' => 'Great Scepter',
				'11' => 'Lord Scepter',
				'12' => 'Great Lord Scepter',
				'13' => 'Divine Scepter Of Archangel',
				'14' => 'Soleil Scepter',
				'15' => 'Shininh Scepter',
				'16' => 'Frost Mace',
				'17' => 'Absolute Scepter',
				'18' => 'Striker Scepter',
				'19' => 'Thunderbolt',
				'20' => 'Horn of Steal',
				'21' => 'Master Scepter',
			),
			3=>array(
				'0' => 'Light Spear',
				'1' => 'Spear',
				'2' => 'Dragon Lance',
				'3' => 'Giant Trident',
				'4' => 'Serpent Spear',
				'5' => 'Double Poleaxe',
				'6' => 'Halberd',
				'7' => 'Berdysh',
				'8' => 'Great Scythe',
				'9' => 'Bill of Balrog',
				'10' => 'Dragon Spear',
				'11' => 'Brova',
				'12' => 'Magmus Peer',
		
			),
			4=>array(
				'0' => 'Short Bow',
				'1' => 'Bow',
				'2' => 'Elven Bow',
				'3' => 'Battle Bow',
				'4' => 'Tiger Bow',
				'5' => 'Silver Bow',
				'6' => 'Chaos Nature Bow',
				'7' => 'Bolt',
				'8' => 'Crossbow',
				'9' => 'Golden Crossbow',
				'10' => 'Arquebus',
				'11' => 'Light Crossbow',
				'12' => 'Serpent Crossbow',
				'13' => 'Bluewing Crossbow',
				'14' => 'Aquagold Crossbow',
				'15' => 'Arrow',
				'16' => 'Saint Crossbow',
				'17' => 'Celestial Bow',
				'18' => 'Divine Crossbow of Archangel',
				'19' => 'Great Reign Crossbow',
				'20' => 'Arrow Viper Bow',
				'21' => 'Sylph Wind Bow',
				'22' => 'Albatross Bow',
				'23' => 'Dark Stinger',
				'24' => 'Aileen Bowr',
				'25' => 'Angelic Bow',
				'26' => 'Devil Crossbow',
				'27' => 'Silver Bow-J',
			),
			5=>array(
				'0' => 'Skull Staff',
				'1' => 'Angelic Staf',
				'2' => 'Serpent Staff',
				'3' => 'Thunder Staff',
				'4' => 'Gorgon Staff',
				'5' => 'Legendary Staff',
				'6' => 'Staff of Resurrection',
				'7' => 'Chaos Lighting Staff',
				'8' => 'Staff Of Destruction',
				'9' => 'Dragon Soul Staff',
				'10' => 'Divine Staff of Archange',
				'11' => 'Staff of Kundun',
				'12' => 'Grand Viper Staff',
				'13' => 'Platina Staff',
				'14' => 'Mystery Stick',
				'15' => 'Violent Wind Stick',
				'16' => 'Red Wing Stick"',
				'17' => 'Ancient Stick',
				'18' => 'Demonic Stick',
				'19' => 'Storm Blitz Stick',
				'20' => 'Eternal Wing Stick',
				'21' => 'Book of Samut',
				'22' => 'Book of Neil',
				'23' => 'Book of Lagle',
				'30' => 'Deadly Staff',
				'31' => 'Inberial Staff',
				'32' => 'Summon Spirit Stick',
				'33' => 'Chrome Staff',
				'34' => 'Raven Stick',
				'35' => 'Raven Stick',
				'36' => 'Divine Stick of the Archangel',
				'37' => 'Spite Staff',
				'38' => 'Legendary Staff-J"',
				'39' => 'Red Wing Stick-J',
				'40' => 'Book of Neil-J',
			),
			6=>array(
				'0' => 'Small Shield',
				'1' => 'Horn Shield',
				'2' => 'Kite Shield',
				'3' => 'Elven Shield',
				'4' => 'Buckler',
				'5' => 'Dragon Slayer Shield',
				'6' => 'Skull Shield',
				'7' => 'Spiked Shield',
				'8' => 'Tower Shield',
				'9' => 'Plate Shield',
				'10' => 'Large Round Shield',
				'11' => 'Serpent Shield',
				'12' => 'Bronze Shield',
				'13' => 'Dragon Shield',
				'14' => 'Legendary Shield',
				'15' => 'Grand Soul Shield',
				'16' => 'Elemental Shield',
				'17' => 'Crimson Glory',
				'18' => 'Salamander Shield',
				'19' => 'Frost Barrier',
				'20' => 'Guardian Shield',
				'21' => 'Cross Shield',
				'22' => 'Lazy Wind Shield',
				'23' => 'Light Lord Shield',
				'24' => 'Dark Devil Shield',
				'25' => 'Magic Knight Shield',
				'26' => 'Ambition Shield',
				'27' => 'Legendary Shield-J',
				'28' => 'Serpent Shield-J"',
			),
			7=>array(
			0  => "Bronze Helm",     
			1 =>  "Dragon Helm"  ,     
			2  => "Pad Helm", 
			3 =>  "Legendary Helm"   , 
			4 =>  "Bone Helm"     ,    
			5 =>  "Leather Helm"   ,   
			6  => "Scale Helm"    ,    
			7  => "Sphinx Mask"    ,   
			8  => "Brass Helm"     ,   
			9  => "Plate Helm"     ,   
			10 =>  "Vine Helm" , 
			11  => "Silk Helm" , 
			12  => "Wind Helm", 
			13  => "Spirit Helm"    ,   
			14  => "Guardian Helm"   , 
			16 =>  "Black Dragon Helm", 
			17 =>  "Dark Phoenix Helm", 
			18  => "Grand Soul Helm" , 
			19  => "Holy Spirit Helm", 
			21 =>  "Great Dragon Helm" , 
			22 =>  "Dark Soul Helm"  , 
			24 =>  "Red Spirit Helm"  , 
			25 =>  "Light Plate Mask", 
			26 =>  "Adamantine Mask", 
			27 =>  "Dark Steel Mask", 
			28 =>  "Dark Master Mask" , 
			29 =>  "Dragon Knight Helm" ,     
			30  => "Venom Mist Helm", 
			31  => "Sylphid Ray Helm", 
			33  => "Sunlight Mask"  , 
			34 =>  "Ashcrow Helm"  , 
			35  => "Eclipse Helm"  ,   
			36  => "Iris Helm"      , 
			38  => "Glorious Mask"    , 
			39  => "Violent Wind Helm" , 
			40  => "Red Wing Helm"   , 
			41  => "Ancient Helm"    , 
			42  => "Demonic Helm"    , 
			43  => "Storm Blitz Helm" , 
			44  => "Eternal Wing Helm" , 
			45  => "Titan  Helm"   ,   
			46  => "Brave Helm"    ,   
			49  => "Seraphim Helm",     
			50  => "Divine Helm"    ,  
			51  => "Royal Mask"   ,  
			52  => "Hades Helm"  ,     
			53 =>  "Succubus Helm" , 
			54 =>  "Lazy Wind Helm"  , 
			55 =>  "Dark Devil Helm" ,  
			56  => "Sticky Helm"    ,  
			58  => "Ambition Mask"  , 
			59  => "Sacred Fire Helm" , 
			60  => "Storm Jahad Helm", 
			61  => "Piercing Groove Helm" ,      
			62 => "Scale Helm"      , 
			63 => "Silk Helm", 
			64 => "Sphinx Helm"     , 
			65 => "Violent Wind Helm" , 
			66 => "Dark Soul Helm"   , 
			67 => "Dragon Helm"    , 
			68 => "Guardian Helm"   , 
			69 => "Legendary Helm"  , 
			70 => "Red Wing Helm"   , 
			72 => "Storm Jahad Helm" , 
			73 =>  "Phoenix Soul Helmet",       
			74 =>  "Stormwing Helm"  , 
			75  => "Light Lord Helmet" , 
			78 => "Dragon Helm-J"  , 
			79 => "Legendary Helm-J" , 
			80 => "Guardian Helm-J" , 
			81 => "Light Plate Mask-J" , 
			82 => "Red Wing Helm-J" , 
			83 => "Sacred Fire Helm-J",                
			),
			8=>array(
			0  => "Bronze Armor",     
			1 =>  "Dragon Armor"  ,     
			2  => "Pad Armor", 
			3 =>  "Legendary Armor"   , 
			4 =>  "Bone Armor"     ,    
			5 =>  "Leather Armor"   ,   
			6  => "Scale Armor"    ,    
			7  => "Sphinx Armor"    ,   
			8  => "Brass Armor"     ,   
			9  => "Plate Armor"     ,   
			10 =>  "Vine Armor" , 
			11  => "Silk Armor" , 
			12  => "Wind Armor", 
			13  => "Spirit Armor"    ,   
			14  => "Guardian Armor"   , 
			16 =>  "Black Dragon Armor", 
			17 =>  "Dark Phoenix Armor", 
			18  => "Grand Soul Armor" , 
			19  => "Holy Spirit Armor", 
			21 =>  "Great Dragon Armor" , 
			22 =>  "Dark Soul Armor"  , 
			24 =>  "Red Spirit Armor"  , 
			25 =>  "Light Plate Armor", 
			26 =>  "Adamantine Armor", 
			27 =>  "Dark Steel Armor", 
			28 =>  "Dark Master Armor" , 
			29 =>  "Dragon Knight Armor" ,     
			30  => "Venom Mist Armor", 
			31  => "Sylphid Ray Armor", 
			33  => "Sunlight Armor"  , 
			34 =>  "Ashcrow Armor"  , 
			35  => "Eclipse Armor"  ,   
			36  => "Iris Armor"      , 
			38  => "Glorious Armor"    , 
			39  => "Violent Wind Armor" , 
			40  => "Red Wing Armor"   , 
			41  => "Ancient Armor"    , 
			42  => "Demonic Armor"    , 
			43  => "Storm Blitz Armor" , 
			44  => "Eternal Wing Armor" , 
			45  => "Titan  Armor"   ,   
			46  => "Brave Armor"    ,   
			49  => "Seraphim Armor",     
			50  => "Divine Armor"    ,  
			51  => "Royal Armor"   ,  
			52  => "Hades Armor"  ,     
			53 =>  "Succubus Armor" , 
			54 =>  "Lazy Wind Armor"  , 
			55 =>  "Dark Devil Armor" ,  
			56  => "Sticky Armor"    ,  
			58  => "Ambition Armor"  , 
			59  => "Sacred Fire Armor" , 
			60  => "Storm Jahad Armor", 
			61  => "Piercing Groove Armor" ,      
			62 => "Scale Armor"      , 
			63 => "Silk Armor", 
			64 => "Sphinx Armor"     , 
			65 => "Violent Wind Armor" , 
			66 => "Dark Soul Armor"   , 
			67 => "Dragon Armor"    , 
			68 => "Guardian Armor"   , 
			69 => "Legendary Armor"  , 
			70 => "Red Wing Armor"   , 
			72 => "Storm Jahad Armor" , 
			73 =>  "Phoenix Soul Armoret",       
			74 =>  "Stormwing Armor"  , 
			75  => "Light Lord Armoret" , 
			78 => "Dragon Armor-J"  , 
			79 => "Legendary Armor-J" , 
			80 => "Guardian Armor-J" , 
			81 => "Light Plate Armor-J" , 
			82 => "Red Wing Armor-J" , 
			83 => "Sacred Fire Armor-J",                
			),
			9=>array(
			0  => "Bronze Pants",     
			1 =>  "Dragon Pants"  ,     
			2  => "Pad Pants", 
			3 =>  "Legendary Pants"   , 
			4 =>  "Bone Pants"     ,    
			5 =>  "Leather Pants"   ,   
			6  => "Scale Pants"    ,    
			7  => "Sphinx Pants"    ,   
			8  => "Brass Pants"     ,   
			9  => "Plate Pants"     ,   
			10 =>  "Vine Pants" , 
			11  => "Silk Pants" , 
			12  => "Wind Pants", 
			13  => "Spirit Pants"    ,   
			14  => "Guardian Pants"   , 
			16 =>  "Black Dragon Pants", 
			17 =>  "Dark Phoenix Pants", 
			18  => "Grand Soul Pants" , 
			19  => "Holy Spirit Pants", 
			21 =>  "Great Dragon Pants" , 
			22 =>  "Dark Soul Pants"  , 
			24 =>  "Red Spirit Pants"  , 
			25 =>  "Light Plate Pants", 
			26 =>  "Adamantine Pants", 
			27 =>  "Dark Steel Pants", 
			28 =>  "Dark Master Pants" , 
			29 =>  "Dragon Knight Pants" ,     
			30  => "Venom Mist Pants", 
			31  => "Sylphid Ray Pants", 
			33  => "Sunlight Pants"  , 
			34 =>  "Ashcrow Pants"  , 
			35  => "Eclipse Pants"  ,   
			36  => "Iris Pants"      , 
			38  => "Glorious Pants"    , 
			39  => "Violent Wind Pants" , 
			40  => "Red Wing Pants"   , 
			41  => "Ancient Pants"    , 
			42  => "Demonic Pants"    , 
			43  => "Storm Blitz Pants" , 
			44  => "Eternal Wing Pants" , 
			45  => "Titan  Pants"   ,   
			46  => "Brave Pants"    ,   
			49  => "Seraphim Pants",     
			50  => "Divine Pants"    ,  
			51  => "Royal Pants"   ,  
			52  => "Hades Pants"  ,     
			53 =>  "Succubus Pants" , 
			54 =>  "Lazy Wind Pants"  , 
			55 =>  "Dark Devil Pants" ,  
			56  => "Sticky Pants"    ,  
			58  => "Ambition Pants"  , 
			59  => "Sacred Fire Pants" , 
			60  => "Storm Jahad Pants", 
			61  => "Piercing Groove Pants" ,      
			62 => "Scale Pants"      , 
			63 => "Silk Pants", 
			64 => "Sphinx Pants"     , 
			65 => "Violent Wind Pants" , 
			66 => "Dark Soul Pants"   , 
			67 => "Dragon Pants"    , 
			68 => "Guardian Pants"   , 
			69 => "Legendary Pants"  , 
			70 => "Red Wing Pants"   , 
			72 => "Storm Jahad Pants" , 
			73 =>  "Phoenix Soul Pantset",       
			74 =>  "Stormwing Pants"  , 
			75  => "Light Lord Pantset" , 
			78 => "Dragon Pants-J"  , 
			79 => "Legendary Pants-J" , 
			80 => "Guardian Pants-J" , 
			81 => "Light Plate Mask-J" , 
			82 => "Red Wing Pants-J" , 
			83 => "Sacred Fire Pants-J",                
			),
			10=>array(
			0  => "Bronze Gloves",     
			1 =>  "Dragon Gloves"  ,     
			2  => "Pad Gloves", 
			3 =>  "Legendary Gloves"   , 
			4 =>  "Bone Gloves"     ,    
			5 =>  "Leather Gloves"   ,   
			6  => "Scale Gloves"    ,    
			7  => "Sphinx Gloves"    ,   
			8  => "Brass Gloves"     ,   
			9  => "Plate Gloves"     ,   
			10 =>  "Vine Gloves" , 
			11  => "Silk Gloves" , 
			12  => "Wind Gloves", 
			13  => "Spirit Gloves"    ,   
			14  => "Guardian Gloves"   , 
			16 =>  "Black Dragon Gloves", 
			17 =>  "Dark Phoenix Gloves", 
			18  => "Grand Soul Gloves" , 
			19  => "Holy Spirit Gloves", 
			21 =>  "Great Dragon Gloves" , 
			22 =>  "Dark Soul Gloves"  , 
			24 =>  "Red Spirit Gloves"  , 
			25 =>  "Light Plate Gloves", 
			26 =>  "Adamantine Gloves", 
			27 =>  "Dark Steel Gloves", 
			28 =>  "Dark Master Gloves" , 
			29 =>  "Dragon Knight Gloves" ,     
			30  => "Venom Mist Gloves", 
			31  => "Sylphid Ray Gloves", 
			33  => "Sunlight Gloves"  , 
			34 =>  "Ashcrow Gloves"  , 
			35  => "Eclipse Gloves"  ,   
			36  => "Iris Gloves"      , 
			38  => "Glorious Gloves"    , 
			39  => "Violent Wind Gloves" , 
			40  => "Red Wing Gloves"   , 
			41  => "Ancient Gloves"    , 
			42  => "Demonic Gloves"    , 
			43  => "Storm Blitz Gloves" , 
			44  => "Eternal Wing Gloves" , 
			45  => "Titan  Gloves"   ,   
			46  => "Brave Gloves"    ,   
			49  => "Seraphim Gloves",     
			50  => "Divine Gloves"    ,  
			51  => "Royal Gloves"   ,  
			52  => "Hades Gloves"  ,     
			53 =>  "Succubus Gloves" , 
			54 =>  "Lazy Wind Gloves"  , 
			55 =>  "Dark Devil Gloves" ,  
			56  => "Sticky Gloves"    ,  
			58  => "Ambition Gloves"  , 
			59  => "Sacred Fire Gloves" , 
			60  => "Storm Jahad Gloves", 
			61  => "Piercing Groove Gloves" ,      
			62 => "Scale Gloves"      , 
			63 => "Silk Gloves", 
			64 => "Sphinx Gloves"     , 
			65 => "Violent Wind Gloves" , 
			66 => "Dark Soul Gloves"   , 
			67 => "Dragon Gloves"    , 
			68 => "Guardian Gloves"   , 
			69 => "Legendary Gloves"  , 
			70 => "Red Wing Gloves"   , 
			72 => "Storm Jahad Gloves" , 
			73 =>  "Phoenix Soul Gloveset",       
			74 =>  "Stormwing Gloves"  , 
			75  => "Light Lord Gloveset" , 
			78 => "Dragon Gloves-J"  , 
			79 => "Legendary Gloves-J" , 
			80 => "Guardian Gloves-J" , 
			81 => "Light Plate Gloves-J" , 
			82 => "Red Wing Gloves-J" , 
			83 => "Sacred Fire Gloves-J",                
			),
			11=>array(
			0  => "Bronze Boots",     
			1 =>  "Dragon Boots"  ,     
			2  => "Pad Boots", 
			3 =>  "Legendary Boots"   , 
			4 =>  "Bone Boots"     ,    
			5 =>  "Leather Boots"   ,   
			6  => "Scale Boots"    ,    
			7  => "Sphinx Boots"    ,   
			8  => "Brass Boots"     ,   
			9  => "Plate Boots"     ,   
			10 =>  "Vine Boots" , 
			11  => "Silk Boots" , 
			12  => "Wind Boots", 
			13  => "Spirit Boots"    ,   
			14  => "Guardian Boots"   , 
			16 =>  "Black Dragon Boots", 
			17 =>  "Dark Phoenix Boots", 
			18  => "Grand Soul Boots" , 
			19  => "Holy Spirit Boots", 
			21 =>  "Great Dragon Boots" , 
			22 =>  "Dark Soul Boots"  , 
			24 =>  "Red Spirit Boots"  , 
			25 =>  "Light Plate Boots", 
			26 =>  "Adamantine Boots", 
			27 =>  "Dark Steel Boots", 
			28 =>  "Dark Master Boots" , 
			29 =>  "Dragon Knight Boots" ,     
			30  => "Venom Mist Boots", 
			31  => "Sylphid Ray Boots", 
			33  => "Sunlight Boots"  , 
			34 =>  "Ashcrow Boots"  , 
			35  => "Eclipse Boots"  ,   
			36  => "Iris Boots"      , 
			38  => "Glorious Boots"    , 
			39  => "Violent Wind Boots" , 
			40  => "Red Wing Boots"   , 
			41  => "Ancient Boots"    , 
			42  => "Demonic Boots"    , 
			43  => "Storm Blitz Boots" , 
			44  => "Eternal Wing Boots" , 
			45  => "Titan  Boots"   ,   
			46  => "Brave Boots"    ,   
			49  => "Seraphim Boots",     
			50  => "Divine Boots"    ,  
			51  => "Royal Boots"   ,  
			52  => "Hades Boots"  ,     
			53 =>  "Succubus Boots" , 
			54 =>  "Lazy Wind Boots"  , 
			55 =>  "Dark Devil Boots" ,  
			56  => "Sticky Boots"    ,  
			58  => "Ambition Boots"  , 
			59  => "Sacred Fire Boots" , 
			60  => "Storm Jahad Boots", 
			61  => "Piercing Groove Boots" ,      
			62 => "Scale Boots"      , 
			63 => "Silk Boots", 
			64 => "Sphinx Boots"     , 
			65 => "Violent Wind Boots" , 
			66 => "Dark Soul Boots"   , 
			67 => "Dragon Boots"    , 
			68 => "Guardian Boots"   , 
			69 => "Legendary Boots"  , 
			70 => "Red Wing Boots"   , 
			72 => "Storm Jahad Boots" , 
			73 =>  "Phoenix Soul Bootset",       
			74 =>  "Stormwing Boots"  , 
			75  => "Light Lord Bootset" , 
			78 => "Dragon Boots-J"  , 
			79 => "Legendary Boots-J" , 
			80 => "Guardian Boots-J" , 
			81 => "Light Plate Boots-J" , 
			82 => "Red Wing Boots-J" , 
			83 => "Sacred Fire Boots-J",                
			),
			12=> array(
			0 => "Wings of Elf" ,                 
			1 => "Wings of Heaven"  ,              
			2 => "Wings of Satan"  ,                 
			3 => "Wings of Spirits"  ,               
			4 => "Wings of Soul"    ,                 
			5 => "Wings of Dragon" ,                  
			6 => "Wings of Darkness"  ,              
			7 => "Orb of Twisting Slash" ,           
			8 => "Healing Orb"          ,             
			9 => "Orb of Greater Fortitude"    ,      
			10 => "Orb of Greater Damage"     ,       
			11 => "Orb of Summoning"          ,       
			12 => "Orb of Rageful Blow"      ,         
			13 => "Orb of Impale"           ,         
			14 => "Orb of Greater Fortitude"  ,        
			15 => "Jewel of Chaos"          ,          
			16 => "Orb of Fire Slash"       ,         
			17 => "Orb of Penetration"     ,           
			18 => "Orb of Ice Arrow"       ,           
			19 => "Orb of Death Stab"     ,           
			21 => "Scroll of FireBurst"    ,           
			22 => "Scroll of Summon"      ,            
			23 => "Scroll of Critical Damage"   ,      
			24 => "Scroll of Electric Spark"   ,       
			26 => "Gem of Secret"       ,              
			30 => "Bundle of Jewel of Bless" ,         
			31 => "Bundle of Jewel of Soul"  ,         
			32 => "Box of Red Ribbon"      ,           
			33 => "Box of Green Ribbon"    ,           
			34 => "Box of Blue Ribbon"    ,           
			35 => "Scroll of Fire Scream"  ,         
			36 => "Wing of Storm"     ,             
			37 => "Wing of Eternal"  ,             
			38 => "Wing of Illusion"  ,               
			39 => "Wing of Ruin"     ,                 
			40 => "Cape of Emperor"  ,               
			41 => "Wing of Curse"   ,                
			42 => "Wind of Despair" ,                
			43 => "Wing of Dimension"  ,            
			44 => "Crystal of Destruction"  ,         
			45 => "Crystal of Multi-Shot"   ,          
			46 => "Crystal of Recovery"    ,           
			47 => "Crystal of Flame Strike"  ,        
			48 => "Scroll of Chaotic Diseier" ,        
			49 => "Cape of Fighter"      ,             
			50 => "Cape of Overrule"    ,              
			60 => "Seed (Fire)"     ,                 
			61 => "Seed (Water)"   ,                   
			62 => "Seed (Ice)"     ,                        
			63 => "Seed (Wind)",
			64 => "Seed (Lightning)"  ,                
			65 => "Seed (Earth)"    ,                
			70 => "Sphere (Mono)"  ,                 
			71 => "Sphere (Di)"        ,              
			72 => "Sphere (Tri)"     ,                
			100 => "Seed Sphere (Fire)"   ,             
			101 => "Seed Sphere (Water)"  ,             
			102 => "Seed Sphere (Ice)"   ,              
			103 => "Seed Sphere (Wind)"   ,           
			104 => "Seed Sphere (Lightning)" ,          
			105 => "Seed Sphere (Earth)"  ,             
			106 => "Seed Sphere (Fire)"   ,            
			107 => "Seed Sphere (Water)" ,             
			108 => "Seed Sphere (Ice)"   ,              
			109 => "Seed Sphere (Wind)"  ,              
			110 => "Seed Sphere (Lightning)" ,        
			111 => "Seed Sphere (Earth)",
			112 => "Seed Sphere (Fire)"    ,           
			113 => "Seed Sphere (Water)"  ,           
			114 => "Seed Sphere (Ice)"   ,           
			115 => "Seed Sphere (Wind)"  ,           
			116 => "Seed Sphere (Lightning)" ,         
			117 => "Seed Sphere (Earth)"   ,         
			130 => "Small Cape of Lord"    ,          
			131 => "Small Wing of Curse"   ,          
			132 => "Small Wings of Elf"   ,             
			133 => "Small Wings of Heaven" ,            
			134 => "Small Wings of Satan"  ,           
			135 => "Little Warrior's Cloak"  ,           
			136 => "Jewel of Life Bundle"   ,           
			137 => "Jewel of Creation Bundle" ,         
			138 => "Jewel of Guardian Bundle" ,         
			139 => "Gemstone Bundle"          ,        
			140 => "Jewel of Harmony Bundle" ,           
			141 => "Jewel of Chaos Bundle"      ,       
			142 => "Lower Refining Stone Bundle" ,      
			143 => "Higher Refining Stone Bundle" ,      
			144 => "Mithril Fragment"   ,              
			145 => "Mithril"            ,               
			146 => "Elixir Fragment"        ,         
			147 => "Elixir"            ,             
			148 => "Mithril Fragment Bunch"  ,        
			149 => "Elixir Fragment Bunch"  ,         
			150 => "Jewel Combination Frame"   ,       
			151 => "Spirit Powder"          ,            
			200 => "Muren's Book of Magic"   ,         
			201 => "Scroll of Etramu"        ,          
			202 => "Lorencia Knights' Iron Shield"  ,  
			203 => "Iron Shield of the Magic"  ,        
			204 => "Hero Elixir"        ,              
			205 => "Brave Hero Elixir"     ,           
			206 => "Gladiator's Dagger"    ,          
			207 => "Merciless Gladiator's Dagger"   ,  
			208 => "Kundun's Madness Blade"      ,      
			209 => "Kundun's Magic Spell Scroll"  ,    
			210 => "Empire Guardians' Stronghold" ,    
			211 => "Ancient Icarus Scroll"  ,          
			212 => "Arca's Prophecy"  ,               
			213 => "Antonia's Sword" ,                
			214 => "Kundun's Seal Scroll" ,           
			221 => "Errtel of Anger"    ,           
			231 => "Errtel of Blessing" ,             
			241 => "Errtel of Integrity" ,            
			251 => "Errtel of Divinity",
			261 => "Errtel of Gale"   ,                
			262 => "Cloak of Death"  ,                  
			263 => "Wings of Chaos" ,                 
			264 => "Wings of Magic" ,                
			265 => "Wings of Life"  ,                 
			266 => "Wings of Conqueror"  ,             
			267 => "Wings of Angel and Devil",       
			),
			14=>array(
				'13' => 'Jewel Of Bless',
				'14' => 'Jewel Of Soul',
				'16' => 'Jewel Of Life',
				'41' => 'Gemstone',
				'42' => 'Jewel Of Harmony',
				'52' => 'Box Of Might',
			),
			15=>array(
				0=>"Scroll of Poison",
				1=>"Scroll of Meteorite",
				2=>"Scroll of Lighting",
				3=>"Scroll of Fire Ball",
				4=>"Scroll of Flame",
				5=>"Scroll of Teleport",
				6=>"Scroll of Ice",
				7=>"Scroll of Twister",
				8=>"Scroll of Evil Spirit",
				9=>"Scroll of Hellfire",
				10=>"Scroll of Power Wave",
				11=>"Scroll of Aqua Beam",
				12=>"Scroll of Cometfall",
				13=>"Scroll of Inferno",
				14=>"Scroll of Teleport Ally",
				15=>"Scroll of Soul Barrier",
				16=>"Scroll of Decay" ,
				17=>"Scroll of Ice Storm",
				18=>"Scroll of Nova",
				19=>"Chain Lightning Parchment",
				20=>"Drain Life Parchment",
				21=>"Lightning Shock Parchment",
				22=>"Damage Reflection Parchment",
				23=>"Berserker Parchment",
				24=>"Sleep Parchment",
				26=>"Weakness Parchment",
				27=>"Innovation Parchment",
				28=>"Scroll of Wizardry Enhance",
				29=>"Scroll of Gigantic Storm",
				30=>"Chain Drive Parchment",
				31=>"Dark Side Parchment",
				32=>"Dragon Roar Parchment",
				33=>"Dragon Slasher Parchment",
				34=>"Ignore Defense Parchment",
				35=>"Increase Health Parchment",
				36=>"Increase Block Parchment",
			),
		);
		if(@in_array($item[$group][$id], $item[$group])){
			return $item[$group][$id];
		} else {
			return "Item Name Unavailable";
		}
	}
	
	
	function listItemsSale(){
		$db = $this->webConn;
		$sql = "SELECT * FROM market ORDER BY id DESC";
		$stmt = sqlsrv_query($db, $sql)or die(print_r(sqlsrv_errors(), true));
		$i = 1;
			while($key = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
				$item = new Items($key['item_hex']);
				$level = $item->getLevel();
				$type = $item->getGroup();
				$id = $item->getId();
				$exe = $item->getExcellent();
				$skill = $item->getSkill();
				$luck = $item->getLuck();
				$opt = $item->getOption();
				$exe_new = "";
				$ac = $item->getAncient();
				$ac_new = "";
				$itemname = $this->getItemName($key['item_hex']);
				switch($ac)
				{
				case 5: case 9:
					{
					if( /*Warrior Leather Set*/
						(($type==7) && ($id==5)) ||          
						(($type==8) && ($id==5)) ||
						(($type==9) && ($id==5)) ||
						(($type==10) && ($id==5)) ||
						(($type==11) && ($id==5)) ||
						(($type==12) && ($id==8)) ||
						(($type==2) && ($id==1))
					) {$itemname = 'Warrior '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Strength +10<br />Incr. attack rate +10<br />Incr. maximum AG +20<br />AG increase Rate + 5<br />Increase Defense + 20<br />Increase agility +10<br />Critical damage rate + 5%<br />Excellent damage rate + 5%<br />Increase strength + 25</font>";}
					elseif( /*Hyperion Bronze Set*/
						(($type==8) && ($id==0)) ||
						(($type==9) && ($id==0)) ||
						(($type==11) && ($id==0)) 
					) {$itemname = 'Hyperion '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Energy + 15<br />Increase Agility + 15<br />Increase skill attacking rate +20<br />Increase Mana + 30</font>";}
					elseif( /*Eplete Scale Set*/
						(($type==7) && ($id==6)) ||
						(($type==8) && ($id==6)) ||
						(($type==9) && ($id==6)) ||
						(($type==6) && ($id==9)) ||
						(($type==12) && ($id==2)) 
					) {$itemname = 'Eplete '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase atack skill + 15<br />Increase atack + 50<br />Increase Wizardry dmg + 5%<br />Maximum HP +50<br />Increase maximum AG +30<br />Incr. Critical damage rate +10%<br />Excellent damage rate +10</font>";}
					elseif( /*Garuda Brass Set*/
						(($type==8) && ($id==8)) ||
						(($type==9) && ($id==8)) ||
						(($type==10) && ($id==8)) ||
						(($type==11) && ($id==8)) ||
						(($type==12) && ($id==13))
					) {$itemname = 'Garuda '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase max stamine + 30<br />Double Damage rate + 5%<br />Energy + 15<br />Maximum HP +50<br />Increase skill attack rate + 25<br />Increase Wizardry Damage + 15%</font>";}
					elseif( /*Kantata Plate Set*/
						(($type==8) && ($id==9)) ||
						(($type==10) && ($id==9)) ||
						(($type==11) && ($id==9)) ||
						(($type==12) && ($id==23)) ||
						(($type==12) && ($id==9))
					) {$itemname = 'Kantata '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Energy + 15<br />Vitality + 30<br />Increase Wizardry Damage + 10%<br />Strength +15<br />Increase skill damage + 25<br />Excellent damage rate + 10%<br />Increase excellent damage + 20</font>";}
					elseif( /*Hyon Dragon Set*/
						(($type==7) && ($id==1)) ||
						(($type==10) && ($id==1)) ||
						(($type==11) && ($id==1)) ||
						(($type==0) && ($id==14))
					) {$itemname = 'Hyon '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase defense + 25<br />2x Dmg rate + 10%<br />Increase skill damage + 20<br />Critical damage rate + 15%<br />Excellent damage rate + 15%<br />Increase Critical Damage + 20<br />Increase Excellent Damage + 20</font>";}
					elseif( /*Apollo Pad Set*/
						(($type==7) && ($id==2)) ||
						(($type==8) && ($id==2)) ||
						(($type==9) && ($id==2)) ||
						(($type==10) && ($id==2)) ||
						(($type==5) && ($id==0)) ||
						(($type==12) && ($id==24)) ||
						(($type==12) && ($id==25))
					) {$itemname = 'Apollo '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Energy + 10<br />Incr. wizardry + 5%<br />Incr. attack. skill + 10<br />Maximum mana + 30<br />Maximum life + 30<br />Incr. max. AG + 20<br />Increase critical damage + 10<br />Increase excellent damage + 10<br />Energy + 30</font>";}
					elseif( /*Evis Bone Set*/
						(($type==8) && ($id==4)) ||
						(($type==9) && ($id==4)) ||
						(($type==11) && ($id==4)) ||
						(($type==12) && ($id==26))
					) {$itemname = 'Evis '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase attacking skill + 15<br />Increase stamina + 20<br />Increase wizardry damage + 10<br />Double damage rate 5%<br />Increase damage success rate +50<br />Increase AG regen rate +5</font>";}
					elseif( /*Hera Sphinx Set*/
						(($type==7) && ($id==7)) ||
						(($type==8) && ($id==7)) ||
						(($type==9) && ($id==7)) ||
						(($type==10) && ($id==7)) ||
						(($type==11) && ($id==7)) ||
						(($type==6) && ($id==6))
					) {$itemname = 'Hera '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Strength + 15<br />Increase wizardry dmg + 10%<br />Increase defensive skill when equipped with shield + 5%<br />Energy + 15<br />Increase attack success rate + 50<br />Critical damage rate + 10%<br />Excellent damage rate + 10%<br />Increase maximum life + 50<br />Increase maximum mana + 50</font>";}
					elseif( /*Anubis Legendary Set*/
						(($type==7) && ($id==3)) ||
						(($type==8) && ($id==3)) ||
						(($type==10) && ($id==3)) ||
						(($type==12) && ($id==21))
					) {$itemname = 'Anubis '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Double Dmg rate + 10%<br />Increase Max mana + 50<br />Increase. Wizardry dmg + 10%<br />Critical damage rate + 15%<br />Excellent damage rate + 15%<br />Increase critical damage + 20<br />Increase excellent damage + 20</font>";}
					elseif( /*Ceto Vine Set*/
						(($type==7) && ($id==10)) ||
						(($type==9) && ($id==10)) ||
						(($type==10) && ($id==10)) ||
						(($type==11) && ($id==10)) ||
						(($type==0) && ($id==2)) ||
						(($type==12) && ($id==22))
					) {$itemname = 'Ceto '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Agility + 10<br />Increase Max HP + 50<br />Increase Def skill + 20<br />Increase defensive skill while using shields + 5%<br />Increase Energy + 10<br />Increases Max HP + 50<br />Increase Strength + 20</font>";}
					elseif( /*Gaia Silk Set*/
						(($type==7) && ($id==11)) ||
						(($type==8) && ($id==11)) ||
						(($type==9) && ($id==11)) ||
						(($type==10) && ($id==11)) ||
						(($type==4) && ($id==9))
					) {$itemname = 'Gaia '.$itemname; 
					$ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font>
					<br />
					<br /><font color=gray>Increase attacking skill + 10
					<br />Increase max mana + 25<br />Power + 10
					<br />Double dmg rate + 5%
					<br />Agility + 30
					<br />Excellent damage rate + 10%
					<br />Increase excellent damage + 10 </font>";}
					elseif( /*Odin Winds Set*/
						(($type==7) && ($id==12)) ||
						(($type==8) && ($id==12)) ||
						(($type==9) && ($id==12)) ||
						(($type==10) && ($id==12)) ||
						(($type==11) && ($id==12))
					) {$itemname = 'Odin '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Energy + 15<br />Increase max life + 50<br />Increase attack success rate + 50<br />Agility + 30<br />Increase maximum mana + 50<br />Ignore enemy defensive skill + 5%<br />Increase maximum AG + 50</font>";}
								
					elseif( /*Argo Spirit Set*/
						(($type==8) && ($id==13)) ||
						(($type==9) && ($id==13)) ||
						(($type==10) && ($id==13))
					) {$itemname = 'Argo '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Agility + 30<br />Power + 30<br />Increase attacking skill + 25<br />Double damage rate + 5%</font>";}
					elseif( /*Gywen Guardian Set*/
						(($type==8) && ($id==14)) ||
						(($type==10) && ($id==14)) ||
						(($type==11) && ($id==14)) ||
						(($type==4) && ($id==5)) ||
						(($type==12) && ($id==28))
					) {$itemname = 'Gywen '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>2x dmg rate + 10%<br />Agility + 30<br />Incr min attacking skill + 20<br />Incr max attacking skill + 20<br />Critical damage rate + 15%<br />Excellent damage rate + 15%<br />Increase critical damage + 20<br />Increase excellent damage + 20</font>";}
					elseif( /*Gaion Storm Crow Set*/
						(($type==8) && ($id==15)) ||
						(($type==9) && ($id==15)) ||
						(($type==11) && ($id==15)) ||
						(($type==12) && ($id==27))
					) {$itemname = 'Gaion '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Ignore enemy defensive skill + 5%<br />2x damage rate + 15%<br />Inc. attacking skill + 15<br />Excellent damage rate + 15%<br />Increase excellent damage + 30<br />Increase wizardry + 20%<br />Increase Strength + 30</font>";}
					elseif( /*Agnis Adamantine Set*/
						(($type==7) && ($id==26)) ||
						(($type==8) && ($id==26)) ||
						(($type==9) && ($id==26)) ||
						(($type==12) && ($id==9))
					) {$itemname = 'Agnis '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Double Damage Rate + 10%<br />Increase Defense +40<br />Increase Skill Damage +20<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Increase Critical Damage +20<br />Increase Excellent Damage +20</font>";}
					elseif( /*Krono Red Wing Set*/
						(($type==7) && ($id==40)) ||
						(($type==9) && ($id==40)) ||
						(($type==10) && ($id==40)) ||
						(($type==12) && ($id==24))
					) {$itemname = 'Chrono '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Double Damage Rate +20%<br />Increase Defense +60<br />Increase Skill Damage +30<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Increase Critical Damage +20<br />Increase Excellent Damage +20</font>";}
					elseif( /*Vega's Sacred Set*/
						(($type==7) && ($id==59)) ||
						(($type==8) && ($id==59)) ||
						(($type==9) && ($id==59)) ||
						(($type==0) && ($id==32))
					) {$itemname = 'Vega '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase damage success rate +50<br />Increase stamina +50<br />Increase max. damage +5<br />Increase excellent damage rate 15%<br />Double damage rate 5%<br />Ignore enemies defensive skill 5%</font>";}
					else {$itemname = 'Ancient '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray></font>";}
					
					
					}
				break;
				case 6: case 10:
					{
					if( /*Obscure Leather Set*/
						(($type==7) && ($id==5)) ||
						(($type==9) && ($id==5)) ||
						(($type==11) && ($id==5)) ||
						(($type==6) && ($id==0))
					) {$itemname = 'Obscure '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase max. life +50<br />Increase agility +50<br />Increase defensive skill when using shield weapons 10%<br />Increase damage +30</font>";}
					elseif( /*Mist Bronze Set*/
						(($type==7) && ($id==0)) ||
						(($type==9) && ($id==0)) ||
						(($type==10) && ($id==0))
					) {$itemname = 'Mist '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase stamina +20<br />Increase skill attacking rate +30<br />Double damage rate 10%<br />Increase agility +20</font>";}
					elseif( /*Berserker Scale Set*/
						(($type==7) && ($id==6)) ||
						(($type==8) && ($id==6)) ||
						(($type==9) && ($id==6)) ||
						(($type==10) && ($id==6)) ||
						(($type==11) && ($id==6))
					) {$itemname = 'Berserker '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase max damage +10<br />Increase max damage +20<br />Increase max damage +30<br />Increase max damage +40<br />Increase skill attacking rate +40<br />Increase strength +40</font>";}
					elseif( /*Cloud Brass Set*/
						(($type==7) && ($id==8)) ||
						(($type==9) && ($id==8)) 
					) {$itemname = 'Cloud '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase critical dmage rate 20%<br />Increase critical damage + 50</font>";}
					elseif( /*Rave Plate Set*/
						(($type==7) && ($id==9)) ||
						(($type==8) && ($id==9)) ||
						(($type==9) && ($id==9)) 
					) {$itemname = 'Rave '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase skill atacking rate + 20<br />Double damage rate 10%<br />Increase damage when using two handed weapon + 30%<br />Ignore enemies defensive skill 5%</font>";}
					elseif( /*Vicious Dragon Set*/
						(($type==7) && ($id==1)) ||
						(($type==8) && ($id==1)) ||
						(($type==9) && ($id==1)) ||
						(($type==12) && ($id==22))
					) {$itemname = 'Vicious '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Skill Damage +15<br />Increased Damage +15<br />Double Damage Rate + 10%<br />Increase Minimum Attack Damage +20<br />Increase Maximum Attack Damage +30<br />Ignore Opponents Defensive Skill + 5%</font>";}
					elseif( /*Banek Pad Set*/
						(($type==7) && ($id==2)) ||
						(($type==9) && ($id==2)) ||
						(($type==11) && ($id==2))
					) {$itemname = 'Banek '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Wizardy Dmg +10%<br />Increase energy +20<br />Increase skill attacking rate +30<br />Increase max. mana +100</font>";}
					elseif( /*Sylion Bone Set*/
						(($type==7) && ($id==4)) ||
						(($type==8) && ($id==4)) ||
						(($type==10) && ($id==4)) ||
						(($type==11) && ($id==4))
					) {$itemname = 'Sylion '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Double damage rate 5%<br />Increase critical damage 5%<br />Increase defensive skill +20<br />Increase strength +50<br />Increase agility +50<br />Increase stamina +50<br />Increase energy +50</font>";}
					elseif( /*Myne Sphinx Set*/
						(($type==8) && ($id==7)) ||
						(($type==9) && ($id==7)) ||
						(($type==11) && ($id==7))
					) {$itemname = 'Myne '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase energy +30<br />Increase defensive skill +30<br />Increase max. mana +100<br />Increase skill atacking rate +15</font>";}
					elseif( /*Isis Legendary Set*/
						(($type==7) && ($id==3)) ||
						(($type==8) && ($id==3)) ||
						(($type==9) && ($id==3)) ||
						(($type==11) && ($id==3))
					) {$itemname = 'Isis '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Skill Damage +10<br />Double Damage Rate + 10%<br />Energy +30<br />Increase Wizardry Damage + 10%<br />Ignore Opponents Defensive Skill + 5%</font>";}
					elseif( /*Drak Vine Set*/
						(($type==7) && ($id==10)) ||
						(($type==8) && ($id==10)) ||
						(($type==9) && ($id==10)) ||
						(($type==11) && ($id==10))
					) {$itemname = 'Drak '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase agility +20<br />Increase damage +25<br />Double damage rate 20%<br />Increase defensive skill +40<br />Increase critical damage rate 10%</font>";}
					elseif( /*Peize Silk Set*/
						(($type==9) && ($id==11)) ||
						(($type==10) && ($id==11)) ||
						(($type==11) && ($id==11))
					) {$itemname = 'Peize '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase max. life +100<br />Increase max. mana +100<br />Increase defensive skill +100</font>";}
					elseif( /*Elvian Wind Set*/
						(($type==9) && ($id==12)) ||
						(($type==11) && ($id==12))
					) {$itemname = 'Elvian '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase agility +30<br />Ignore enemies defensive skill 5%</font>";}
					elseif( /*Karis Spirit Set*/
						(($type==7) && ($id==13)) ||
						(($type==9) && ($id==13)) ||
						(($type==11) && ($id==13))
					) {$itemname = 'Karis '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase skill attacking rate +15<br />Double damage rate 10%<br />Increase critical damage rate 10%<br />Increase agility +40</font>";}
					elseif( /*Aruane Guardian Set*/
						(($type==7) && ($id==14)) ||
						(($type==8) && ($id==14)) ||
						(($type==9) && ($id==14)) ||
						(($type==11) && ($id==14))
					) {$itemname = 'Aruane '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increased Damage +10<br />Double Damage Rate + 10%<br />Increase Skill Damage +20<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Ignore Opponents Defensive Skill + 5%</font>";}
					elseif( /*Muren Storm Crow Set*/
						(($type==8) && ($id==15)) ||
						(($type==9) && ($id==15)) ||
						(($type==10) && ($id==15)) ||
						(($type==12) && ($id==21))
					) {$itemname = 'Muren '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Skill Damage +10<br />Increase Wizardry Damage + 10%<br />Double Damage Rate + 10%<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Increase Defense +25<br />Increase Two-handed Weapon Equipment Damage + 20%</font>";}
					elseif( /*Browii Adamantine Set*/
						(($type==9) && ($id==26)) ||
						(($type==10) && ($id==26)) ||
						(($type==11) && ($id==26)) ||
						(($type==12) && ($id==25))
					) {$itemname = 'Browii '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Damage +20<br />Increase Skill Damage +20<br />Increase Energy +30<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Ignore Opponents Defensive Skill + 5%<br />Increase Command +30</font>";}
					elseif( /*Semeden Red Wing Set*/
						(($type==7) && ($id==40)) ||
						(($type==8) && ($id==40)) ||
						(($type==10) && ($id==40)) ||
						(($type==11) && ($id==40))
					) {$itemname = 'Semeden '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase Wizardry + 15%<br />Increase Skill Damage +25<br />Increase Energy +30<br />Increase Critical Damage Rate + 15%<br />Increase Excellent Damage Rate + 15%<br />Ignore Opponents Defensive Skill + 5%</font>";}
					elseif( /*Chamereuui's Sacred Set*/
						(($type==8) && ($id==59)) ||
						(($type==9) && ($id==59)) ||
						(($type==0) && ($id==32)) ||
						(($type==11) && ($id==59))
					) {$itemname = 'Chamereuui '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray>Increase defensiva skill +50<br />Double damage rate 5%<br />Increase damage +30<br />Increase excellent damage rate 15%<br />Increase skill attacking rate +30<br />Increase excellent damage +20</font>";}
					else {$itemname = 'Ancient '.$itemname; $ac_new = "<br /> <font color=#F4CB3F>Set Item Option Info</font><br /> <font color=gray></font>";}
					
					}
				break;
				}
				if($level > 0) { $level = " + ".$item->getLevel(); } else { $level = ""; }
				if($exe > 0){ 
					$class = "title";
					if($item->getGroup() > 6) {
						if ($exe >= 32) { $exe -= 32; $exe_new .= "Increase MaxHP +4% <br />"; }
						if ($exe >= 16) { $exe -= 16; $exe_new .= "Increase MaxMana +4% <br />"; }
						if ($exe >= 8) { $exe -= 8; $exe_new .= "Damage Decrease +4% <br />";  }
						if ($exe >= 4) { $exe -= 4; $exe_new .= "Reflect damage +5% <br />";  }
						if ($exe >= 2) { $exe -= 2; $exe_new .= "Defense success rate +10% <br />"; }
						if ($exe >= 1) { $exe -= 1; $exe_new .= "Increase Zen After Hunt +40% <br />";  }
					}elseif($item->getGroup() > 11){
						if ($exe >= 32) { $exe -= 32; $exe_new .= ""; }
						if ($exe >= 16) { $exe -= 16; $exe_new .= "Increase Attacking Speed +5 <br />"; }
						if ($exe >= 8) { $exe -= 8; $exe_new .= "Increase Stamina +50 <br />";  }
						if ($exe >= 4) { $exe -= 4; $exe_new .= "Ignores 3% of your opponent's armor <br />";  }
						if ($exe >= 2) { $exe -= 2; $exe_new .= "Increased Mana + ".(50+(5*$level))." <br />"; }
						if ($exe >= 1) { $exe -= 1; $exe_new .= "Increased Health + ".(50+(5*$level))." <br />";  }
					}else {
						if ($exe >= 32) { $exe -= 32; $exe_new .= "Excellent Damage Rate + 10% <br />"; }
						if ($exe >= 16) { $exe -= 16; $exe_new .= "Increase Damage + 2% <br />"; }
						if ($exe >= 8) { $exe -= 8; $exe_new .= "Increase Speed + 7 <br />";  }
						if ($exe >= 4) { $exe -= 4; $exe_new .= "Increase Damage + level/20 <br />";  }
						if ($exe >= 2) { $exe -= 2; $exe_new .= "Increase life after monster + life/8 <br />"; }
						if ($exe >= 1) { $exe -= 1; $exe_new .= "Increase mana after monster + mana/8 <br />";  }
					}
				} 
				else { $class= ""; $exe_new = ""; }
				if($skill > 0) { $skill = " + Skill <br />"; } else { $skill = ""; }
				if($luck > 0) { $luck = "Luck (success rate of Jewel of Soul +25%) <br />Luck (critical damage rate +5%) <br />"; } else { $luck = ""; }
				if($opt > 0) {$opt_new = "+ Option ".$opt * 4 ." <br />"; } else {$opt_new = '';}
				echo "<form action = '' method = 'post'>";
				echo "
				<tr>
				<td>
				<p class='$class'>
				".$itemname." ".$level."
				</p>
				<span class= 'opt'>
				$luck
				$skill
				$opt_new
				</span>
				<p class = 'title'>$exe_new</p>
				<p class = 'title'>$ac_new</p>
				</td>";
					echo "<input type = 'hidden' name = 'item' value = '".$key['item_hex']."'/>";
					echo "<td>".$key['price1']."</td>";
					echo "<input type = 'hidden' name = 'price1' value = '".$key['price1']."'/>";
					echo "<td>".$key['price2']."</td>";
					echo "<input type = 'hidden' name = 'price2' value = '".$key['price2']."'/>";
					echo "<td>".$key['seller']."</td>";
					echo "<input type = 'hidden' name = 'seller' value = '".$key['seller']."'/>";
					echo "<td>";
					if($_SESSION['user'] == $key['seller']){
						echo "<input type ='submit' name = 'cancel' value = 'Cancel' class ='ok'/>";
					} else {
						echo "<p><input type = 'number' name = 'place_bid' palaceholder = 'Enter Bid' style = 'width:80px;' class = 'input' max = '650000' min = '0'/></p>";
						echo "<p><input type = 'submit' name = 'bid' value = 'Place Bid' class ='ok'/> </p>";
						echo "<p><input type = 'submit' name = 'buy' value = 'Buy Now' class ='ok'/></p>";
						echo "<input type = 'hidden' name = 'buyer' value = '".$_SESSION['user']."'/>";
					}
					echo "</td>";
				echo "</tr>";
				echo "</form>";
			}
	}
	
	function buyItem($item,$buyer,$price1,$price2){

		$db = $this->webConn;
		$db2 = $this->gameConn;
		$sql = "SELECT * FROM market WHERE item_hex = '".$item."'";
		$stmt = sqlsrv_query($db,$sql);
		$fetch = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC);
		
		$seller = $fetch['seller'];
		
		if($this->xShopData($buyer,"WCoinC") >= $price2) {
		sqlsrv_query($db,"INSERT INTO online_warehouse (AccountID,item_hex) VALUES ('".$buyer."','".$item."')")or die(print_r(sqlsrv_errors(), true));
		sqlsrv_query($db2,"UPDATE CashShopData SET WCoinC = WCoinC - $price2 WHERE AccountID = '".$buyer."'")or die(print_r(sqlsrv_errors(), true));
		sqlsrv_query($db2,"UPDATE CashShopData SET WCoinC = WCoinC + $price2 WHERE AccountID = '".$seller."'")or die(print_r(sqlsrv_errors(), true));
		sqlsrv_query($db,"DELETE FROM market WHERE item_hex = '".$item."'")or die(print_r(sqlsrv_errors(), true));
		
		$this->errorHandle("Item bought : ".$this->getItemName($item));
		
		} else {
			$this->errorHandle("Not enough credits");
		}
		
	}
	
	function errorHandle($error) {
		echo "
		 <script type='text/javascript'>
                alert('$error');
                
                </script>
		";
	}
	
	
	
	function top15 (){
		$db = $this->gameConn;
		$sql = "SELECT TOP 10 * FROM Character 
		WHERE AccountID != 'vitrex' AND AccountID != 'krutoyed' AND AccountID != 'baranka24'
			ORDER BY GrandResetCount DESC,ResetCount DESC,cLevel DESC";
		$stmt = sqlsrv_query($db, $sql)or die(print_r(sqlsrv_errors(), true));
		$i = 0;
			while($key = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
				if($key['GrandResetCount'] == NULL) {
					$grr = 0;
				} else {
					$grr = $key['GrandResetCount'];
				}
				
				if($key['RebornCount'] == NULL) {
					$reb = 0;
				} else {
					$reb = $key['RebornCount'];
				}
				
				echo "<tr>";
				$i++;
					echo "<td>$i.</td>";
					echo "<td>".$key['Name']."</td>";
					echo "<td>".$key['cLevel']."</td>";
					echo "<td>[ <span class = 'started'>".$reb."</span> ] [ <span class = 'started'>".$grr."</span> ] [ <span class = 'notstarted'>". $key['ResetCount'] ."</span> ]</td>";
					echo "<td>".$this->getClassName($key['Class'])."</td>";
				echo "</tr>";
			}

	}
		
	function top150 (){
		$db = $this->gameConn;
		$sql = "SELECT TOP 150 * FROM Character 
		WHERE AccountID != 'vitrex' AND AccountID != 'krutoyed' AND AccountID != 'baranka24'
			ORDER BY GrandResetCount DESC,ResetCount DESC,cLevel DESC";
		$stmt = sqlsrv_query($db, $sql)or die(print_r(sqlsrv_errors(), true));
		$i = 0;
			while($key = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
				
			$sql2 = sqlsrv_query($db,"SELECT * FROM MEMB_STAT WHERE memb___id = '".$key['AccountID']."'");
			$fetch = sqlsrv_fetch_array($sql2,SQLSRV_FETCH_ASSOC);
			$result2 = sqlsrv_query($db,"SELECT * FROM AccountCharacter WHERE GameIDC = '".$key['Name']."'");
			$fetch2 = sqlsrv_fetch_array($result2,SQLSRV_FETCH_ASSOC);
			// guild
			$sql3 = "SELECT * FROM GuildMember WHERE Name = '".$key['Name']."'";
			$stmt3 = sqlsrv_query($db, $sql3, array(), array("Scrollable"=>SQLSRV_CURSOR_KEYSET));
			$fetch3 = sqlsrv_fetch_array($stmt3,SQLSRV_FETCH_ASSOC);
			
			$guild = isset($fetch3['G_Name']) ? $fetch3['G_Name'] : " - ";
			
				if($key['GrandResetCount'] == NULL) {
					$grr = 0;
				} else {
					$grr = $key['GrandResetCount'];
				}	
				
				if($key['RebornCount'] == NULL) {
					$reb = 0;
				} else {
					
					$reb = $key['RebornCount'];
				}
				echo "<tr>";
				$i++;
					echo "<td>$i.</td>";
					echo "<td>".$key['Name']."</td>";
					echo "<td>".$this->getClassName($key['Class'])."</td>";
					echo "<td>$guild</td>";
					echo "<td>".$key['cLevel']."</td>";
					echo "<td>".$key['ResetCount']."</td>";
					echo "<td>".$grr."</td>";
					echo "<td>".$reb."</td>";
				if($fetch['ConnectStat'] == 1 && $fetch2['GameIDC'] == $key['Name']){
					$status = "<span class = 'title'>Online</span>";
				} else {
					$status = "<span class = 'notstarted'>Offline</span>";
				}
					echo "<td>$status</td>";
				echo "</tr>";
			}

	}
		
	function top150Money(){
		$db = $this->gameConn;
		$sql = "SELECT TOP 150 * FROM Character 
		WHERE AccountID != 'vitrex' AND AccountID != 'krutoyed' AND AccountID != 'baranka24'
			ORDER BY Money DESC";
		$stmt = sqlsrv_query($db, $sql)or die(print_r(sqlsrv_errors(), true));
		$i = 0;
			while($key = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
			// guild
			$sql3 = "SELECT * FROM GuildMember WHERE Name = '".$key['Name']."'";
			$stmt3 = sqlsrv_query($db, $sql3, array(), array("Scrollable"=>SQLSRV_CURSOR_KEYSET));
			$fetch2 = sqlsrv_fetch_array($stmt3,SQLSRV_FETCH_ASSOC);
			
			$guild = isset($fetch2['G_Name']) ? $fetch2['G_Name'] : " - ";
			
				if($key['GrandResetCount'] == NULL) {
					$grr = 0;
				} else {
					$grr = $key['GrandResetCount'];
				}	
				
				if($key['RebornCount'] == NULL) {
					$reb = 0;
				} else {
					
					$reb = $key['RebornCount'];
				}
				echo "<tr>";
				$i++;
					echo "<td>$i.</td>";
					echo "<td>".$key['Name']."</td>";
					echo "<td>".$this->getClassName($key['Class'])."</td>";
					echo "<td>$guild</td>";
					echo "<td>".$key['cLevel']."</td>";
					echo "<td>".$key['ResetCount']."</td>";
					echo "<td>".$grr."</td>";
					echo "<td>".$reb."</td>";
					echo "<td class = 'title'>".number_format($key['Money'])."</td>";
				echo "</tr>";
			}

	}
		
	function top150Pk(){
		$db = $this->gameConn;
		$sql = "SELECT TOP 150 * FROM Character 
		WHERE AccountID != 'vitrex' AND AccountID != 'krutoyed' AND AccountID != 'baranka24'
			ORDER BY PkCount DESC";
		$stmt = sqlsrv_query($db, $sql)or die(print_r(sqlsrv_errors(), true));
		$i = 0;
			while($key = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
			// guild
			$sql3 = "SELECT * FROM GuildMember WHERE Name = '".$key['Name']."'";
			$stmt3 = sqlsrv_query($db, $sql3, array(), array("Scrollable"=>SQLSRV_CURSOR_KEYSET));
			$fetch2 = sqlsrv_fetch_array($stmt3,SQLSRV_FETCH_ASSOC);
			
			$guild = isset($fetch2['G_Name']) ? $fetch2['G_Name'] : " - ";
			
				if($key['GrandResetCount'] == NULL) {
					$grr = 0;
				} else {
					$grr = $key['GrandResetCount'];
				}	
				
				if($key['RebornCount'] == NULL) {
					$reb = 0;
				} else {
					
					$reb = $key['RebornCount'];
				}
				echo "<tr>";
				$i++;
					echo "<td>$i.</td>";
					echo "<td>".$key['Name']."</td>";
					echo "<td>".$this->getClassName($key['Class'])."</td>";
					echo "<td>$guild</td>";
					echo "<td>".$key['cLevel']."</td>";
					echo "<td>".$key['ResetCount']."</td>";
					echo "<td>".$grr."</td>";
					echo "<td>".$reb."</td>";
					echo "<td class = 'red'>".$key['PkCount']."</td>";
				echo "</tr>";
			}

	}
	
	function sendRepot($title,$content,$cat,$url,$poster){
		$db = $this->webConn;
		$sql = "INSERT INTO bugs (title,bug,cat,date,poster,url) 
			VALUES ('".$this->clean($title)."','".$this->clean($content)."','".$this->clean($cat)."','".date("Y-m-d , H:m:s")."','".$this->clean($poster)."','".htmlspecialchars($url)."');";
		$stmt = sqlsrv_query($db, $sql)or die(print_r(sqlsrv_errors(), true));
	}
	
	function top15Guilds (){
		$db = $this->gameConn;
		$sql = "SELECT TOP 5 * FROM Guild 
			ORDER BY G_Score DESC";
		$stmt = sqlsrv_query($db, $sql)or die(print_r(sqlsrv_errors(), true));
		$i = 0;
			
			while($key = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
				
			$sql2 = "SELECT * FROM GuildMember WHERE G_Name = '".$key['G_Name']."'";
			$stmt2 = sqlsrv_query($db, $sql2, array(), array("Scrollable"=>SQLSRV_CURSOR_KEYSET));
			$fetch = sqlsrv_num_rows($stmt2);
				echo "<tr>";
				$i++;
					echo "<td>$i.</td>";
					echo "<td>".$key['G_Name']."</td>";
					echo "<td>".$key['G_Score']."</td>";
					echo "<td>".$fetch."</td>";
				echo "</tr>";
			}

	}
		
	function top150Guild(){
		$db = $this->gameConn;
		$sql = "SELECT TOP 150 * FROM Guild 
			ORDER BY G_Score DESC";
		$stmt = sqlsrv_query($db, $sql)or die(print_r(sqlsrv_errors(), true));
		$i = 0;
			
			while($key = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
				
			$sql2 = "SELECT * FROM GuildMember WHERE G_Name = '".$key['G_Name']."'";
			$stmt2 = sqlsrv_query($db, $sql2, array(), array("Scrollable"=>SQLSRV_CURSOR_KEYSET));
			$fetch = sqlsrv_num_rows($stmt2);
				echo "<tr>";
				$i++;
					echo "<td>$i.</td>";
					echo "<td>".$key['G_Name']."</td>";
					echo "<td>".$key['G_Score']."</td>";
					echo "<td>".$fetch."</td>";
					echo "<td class = 'title'>".$key['G_Master']."</td>";
				echo "</tr>";
			}

	}
	
	
	function adminsOnline(){
		$db = $this->gameConn;
		$sql = "SELECT * FROM MEMB_STAT WHERE memb___id = 'vitrex' OR memb___id = 'Krutoyed'";
		$stmt = sqlsrv_query($db, $sql)or die(print_r(sqlsrv_errors(), true));
		$i = 0;
			
			while($key = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
				
			
			$char_query = sqlsrv_query($db,"SELECT * FROM AccountCharacter WHERE Id = '".$key['memb___id']."'");
			$fetch = sqlsrv_fetch_array($char_query,SQLSRV_FETCH_ASSOC);
			
				echo "<tr>";
				$i++;
					echo "<td>$i.</td>";
					echo "<td>".$fetch['GameIDC']."</td>";
					if($key['ConnectStat'] == 1) {
						$status = "<span class = 'title'>Online</span>";
					} else {
						$status = "Offline";
					}
					echo "<td>".$status."</td>";
					echo "<td>Administrator</td>";
				echo "</tr>";
				
			}
	}
	
	function getClassName($value){
		if ($value == 0) {$class = 'DW';} 
		if ($value == 1) {$class = 'SM';} 
		if ($value == 2) {$class = 'GM';} 
		if ($value == 16){$class = 'DK';} 
		if ($value == 17){$class = 'BK';} 
		if ($value == 18){$class = 'BM';} 
		if ($value == 32){$class = 'Elf';} 
		if ($value == 33){$class = 'ME';} 
		if ($value == 34){$class = 'HE';} 
		if ($value == 48){$class = 'MG';} 
		if ($value == 50){$class = 'DM';} 
		if ($value == 64){$class = 'DL';} 
		if ($value == 80){$class = 'SUM';} 
		if ($value == 81){$class = 'BS';} 
		if ($value == 82){$class = 'DS';} 
		if ($value == 96){$class = 'RF';} 
		if ($value == 66){$class = 'LE';} 
		if ($value == 97){$class = 'FM';} 
		
		return $class;
	}
	
	
	function getCharData($name,$row){
		$db = $this->gameConn;
		$sql = "SELECT * FROM Character 
			WHERE 
			AccountID='".$this->clean($name)."'";
		$stmt = sqlsrv_query($db, $sql, array(), array("Scrollable"=>SQLSRV_CURSOR_KEYSET))or die(print_r(sqlsrv_errors(), true));
		$fetch = sqlsrv_fetch_array($stmt);
		
		return $fetch[$row];
	}
		function getCharDataV2($name,$row){
		$db = $this->gameConn;
		$sql = "SELECT * FROM Character 
			WHERE 
			Name='".$this->clean($name)."'";
		$stmt = sqlsrv_query($db, $sql, array(), array("Scrollable"=>SQLSRV_CURSOR_KEYSET))or die(print_r(sqlsrv_errors(), true));
		$fetch = sqlsrv_fetch_array($stmt);
		
		return $fetch[$row];
	}
	
	function login($username,$password){
		$db = $this->webConn;
		$sql = "SELECT * FROM users 
			WHERE 
			username='".$this->clean($username)."' 
			AND 
			password='".$this->clean($password)."'
			";
		$stmt = sqlsrv_query($db, $sql, array(), array("Scrollable"=>SQLSRV_CURSOR_KEYSET))or die(print_r(sqlsrv_errors(), true));
		$row_count = sqlsrv_num_rows($stmt);
		
		if($row_count > 0) {
			Header("location:index.php");
			$_SESSION['user'] = $this->clean($username);
		} else {
			$result = "Wrong account details, please try again.";
		}
		
		echo $result;
		

	}
	
	function fetchUser($data){
		$db = $this->webConn;
		$sql = "SELECT * FROM users WHERE 
			username='".$this->clean($_SESSION['user'])."'";
		$stmt = sqlsrv_query($db,$sql);
		$result = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC);
		return $result[$data];

	}
	
	function webConn(){
		$conn = sqlsrv_connect('localhost', array('Database'=>'Website','UID'=> 'sa','PWD'=> 'Miraclemu2016',"CharacterSet" =>"UTF-8","ConnectionPooling" => "0"
			,"MultipleActiveResultSets"=>'1'
		))or die(print_r(sqlsrv_errors(), true));

		return $conn;
	}
	
	function gameConn(){
		$conn = sqlsrv_connect('localhost', array('Database'=>'MuOnline','UID'=> 'sa','PWD'=> 'Miraclemu2016',"CharacterSet" =>"UTF-8","ConnectionPooling" => "0"
			,"MultipleActiveResultSets"=>'1'
		))or die(print_r(sqlsrv_errors(), true));

		return $conn;
	}
	
	function clean($str) {
	   if(get_magic_quotes_gpc())
	   {
		$str= stripslashes($str);
	   }
	   return str_replace("'", "''", $str);
	}
	
}