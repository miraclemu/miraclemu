<?php 
	
	$conf = array(
		'char'=> array(
		//general
			'max_stats' => 65000,
			'onlineHoursRatio'=>3,
		// resets
			'resetZenCost'=> 1000000,
			'resetLevel' => 400,
			'pointsAfterReset' => 500,
			'resetReward' => 5,
		// grand resets
			'grandResetLevel' => 400,
			'grandResetReset' => 100,
			'grandResetZenCost' => 100000000,
			'grandResetReward' => 500,
		),
		'web'=>array(
			'register'=>'0'
		),
	);