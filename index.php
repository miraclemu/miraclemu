<?php
session_start();
ob_start();
require_once 'engine/functions.php';
$sys = new system();
include 'tpl/header.php';
?>
<div class = "body">
	<div class = "left_side">
		<div class= "panel_top">Account Area</div>
		<div class = "panel_body">
		<? if(!isset($_SESSION['user'])) { ?>
		<p class = "red"><?php if(isset($_POST['submit'])) { $sys->login($_POST['username'],$_POST['password']); } ?></p>
			<form action = "" method = "POST">
				<input type = "text" name = "username" placeholder = "Username" class = "input" required/>
				<br /><br />
				<input type = "password" name = "password" placeholder = "**********" class = "input" required/>
				<br /><br />
				<input type = "submit" name = "submit" value = "Login" class = "ok" required/>
				Don't have account ? <a href = "register.php" class = "title">Register</a>  !
			</form>
		<? } else { 

					$sys->levelUp();

			$xp_percent = $sys->fetchUser("xp") * 100 / $sys->xpForLevel($sys->fetchUser("lvl"));

				echo "Welcome <span class = 'red'>".$sys->fetchUser("username")."</span>";
				echo "<br />";
				echo "Account level [ ".$sys->fetchUser("lvl")." ]";
				echo "<br />";
				echo "<table width = '100%' border = '0' cellpading = '0' cellspacing = '0'>";
					echo "<tr><td width = '100%' style = 'background-color:#34363e; height: 10px;'>
						<div style = 'width:".$xp_percent."%; background-color:#0076a3; height:10px; color:#fff; line-height:10px;text-align:center;'>
						".round($xp_percent)." % </div>
					</td></tr>";
				echo "</table>";
				echo "<br />".$sys->fetchUser("xp")." / ".$sys->xpForLevel($sys->fetchUser("lvl"));
				echo "<hr />";

					if($sys->isOnline($_SESSION['user']) == 1){
						echo "Account : <span class = 'title'>Online</span> - Client offline? <a href = 'accfix.php'>Click here</a>";
					} else {
						echo "Account : <span class = 'red'>Offline</span>";
					}

				echo "<hr />";
					echo "<table width = '100%' cellpading = '0' cellspacing = '0'>";
					echo "<tr>
						<td>Credits</td>
						<td>[ ".$sys->xShopData($_SESSION['user'],"WCoinC")." ]</td>

						<td>Silver Coins</td>
						<td>[ ".$sys->xShopData($_SESSION['user'],"WCoinP")." ]</td>
					</tr>";
				echo "</table>";
				echo "<hr />";
			?>

			<div class = "acc_menu">
				<a href = "characters.php" class = "acc_menu_item">Characters</a>
				<a href = "webshop.php" class = "acc_menu_item">Webstore</a>
				<a href = "accfix.php" class = "acc_menu_item">Account Fixer</a>
				<a href = "charfix.php" class = "acc_menu_item">Unstuck Character</a>
			</div>
			<hr />
			<a href = "logout.php" class = "red">Logout</a>
		<?

		} ?>
		</div>

		<div class= "panel_top">Top 10 Characters</div>
		<div class = "panel_body">
			<table width = "100%" align = "center" cellpading = '0' cellspacing = '1' class = 'stats' >
			<tr>
				<td>#</td>
				<td>Name</td>
				<td>Level</td>
				<td>[REB][GR][RR]</td>
				<td>Class</td>
			</tr>
			<?php $sys->top15(); ?>
		</table>
		</div>

		<div class= "panel_top">Top 5 Guilds</div>
		<div class = "panel_body">
			<table width = "100%" cellpading = '0' cellspacing = '1' class = "stats">
			<tr>
				<td>#</td>
				<td>Name</td>
				<td>Score</td>
				<td>Members</td>
			</tr>
			<?php $sys->top15Guilds(); ?>
		</table>
		</div>

		<div class= "panel_top">Staff Status</div>
			<div class = "panel_body">
				<table width = "100%" cellpading = '0' cellspacing = '1' class = "stats">
				<tr>
					<td>#</td>
					<td>Name</td>
					<td>Status</td>
					<td>Rank</td>
				</tr>
				<?php $sys->adminsOnline(); ?>
			</table>
		</div>

		<div class = "clear"></div>

		<div class= "panel_top">Social Media & Contacts</div>
		<div class = "panel_body">

		<br />

			<div class = "yt">

				<a href = '#'>Youtube</a>

			</div>

			<div class = "fb">

				<div class="fb-like" data-href="https://www.facebook.com/MiraclemuEU/" data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div>

			</div>

			<div class = "sk">

				<a href="skype:jakbut?chat">Skype</a>

			</div>

		</div>

		<div class= "panel_top">Website Statistics</div>
			<div class = "panel_body">
				Visitors By Country <br />
				<a href="http://s11.flagcounter.com/more/Nm0">
				<img src="http://s11.flagcounter.com/count2/Nm0/bg_FFFFFF/txt_000000/border_CCCCCC/columns_4/maxflags_20/viewers_0/labels_0/pageviews_0/flags_0/percent_0/" alt="Flag Counter" border="0">
				</a>
				Genreal Unique Visits.<br />
				<a target="_blank" href="http://www.hey.lt/details.php?id=miraclemu"><img width="88" height="31" border="0" src="http://www.hey.lt/count.php?id=miraclemu" alt="Hey.lt - Nemokamas lankytoju skaitliukas"></a>

			</div>

	</div>

	<div class = "right_side">
	<h2>Need shiny new equipment? Visit <a href = 'market.php'>Market</a>. But before read guide how to use it <a href = 'http://www.miraclemu.eu/forum/showthread.php?107-How-to-use-market'>here</a></h2>
		<div class ="text_top">
			Server Information <span class = 'red'></span>
		</div>

		<img src = "../tpl/images/server_on.png" align ="left" title = "Server Is Online" alt = "server online">

				<div style = "padding-top:2px">
					&nbsp;Server rates : 500 x <br />
					&nbsp;Players online : <? echo $sys->PlayersOnline(); ?><br />
					&nbsp;Server season: Ex803 <br />
				</div>
		<div class ="clear"></div>

		<div class ="text_top title">
			Important & Useful Tips
		</div>
			<ul>
				<li>Administration Never asks for your personal account details, we strongly recommend to
				not give your account details to anyone even if you get messages from the administrators.</li>
				<li>Party bonus helps you level up faster.</li>
				<li>Dont forget trade your online played hours to in game rewards.</li>
				<li>Don't forget to visit our <a href = "forum/" class ='title'>FORUM</a>.</li>
				<li>Making Resets in website awards you with Account Experience & Credits.</li>
				<li>Learn more about the account levelling system, since it's new feature never seen before <a href = "/forum" class ='title'>learn more</a>.</li>
				<li>Make sure you know server rules before you entering the game, Ignorance does not exempt from responsibility <a href = "rules.php" class ='title'>Rules</a>.</li>
			</ul>
		<div class ="text_top title">
			Server News & Changes
		</div>

		<div class= "text_body">
		<div class = "new_title">New essential patch for xShop !</div>
			<p>
			New patch for xShop MUST be downloaded, url - <a href = 'https://mega.nz/#!5xBEhKrL!ez-BcR6z0j08T5G81f8GIQmooFKMVonvGJCkoKdwXfA' class = 'red'>HERE</a>
			<br />
			How to install patch? just move and replace all files in MiracleMu folder with files from patch.
			</p>
		</div>

		<div class= "text_body">
		<div class = "new_title">Custom event !</div>
			<p>
				Custom even started. We are lurking for unique suggestions about 380 exe items. have one ? post them
				<a href = 'http://www.miraclemu.eu/forum/forumdisplay.php?4-Server-Suggestions' class = 'red'>HERE</a>
	Best idea winner will win 380 level excelent weapon with 3 selected options for his class !
			</p>
		</div>

		<div class= "text_body">
		<div class = "new_title">Few staff positions available.</div>
			<p>
				Wanna become part of MiracleMu team? what are you waiting for , visit
				<a href = 'http://www.miraclemu.eu/forum/showthread.php?112-Staff-Member-positions-available&p=119#post119' class = 'red'>this</a> page an write an application for position
				that more suits you !!!
			</p>
		</div>

		<div class ="text_top title">
			Server Events & Announcements
		</div>
		<div class= "text_body">
		<div class = "new_title">Beta Finished</div>
		<p>
			<a href = "changelog.txt" class = "red">CHANGELOG FILE - <? echo "Lastest changes: " . date ("F d Y", filemtime("changelog.txt")); ?></a>
			</p>
			</div>




	</div>
</div>
<? include 'tpl/footer.php';
