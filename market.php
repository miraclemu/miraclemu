<?php 
session_start(); 
ob_start(); 
require_once 'engine/functions.php';
$sys = new system();
include 'tpl/header.php';
?>
<div class = 'body'>
<?
if(!isset($_SESSION['user'])) {
	echo "<p class = 'red'>You have to log in to user market system.</p>";
} else {
		echo "<p><a href = 'market.php?p=my' class = 'title'>My Items</a>";
		echo " / ";
		echo "<a href = 'market.php?p=items' class = 'title'>Items For Sell</a>";	
		echo " / ";
		echo "<a href = 'market.php?p=bank' class = 'title'>Online Bank</a></p>";
		if(isset($_GET['p'])) {
			switch($_GET['p']) {
				
				case "bank":
					echo "<p class = 'red'>All items you bought , gifts, and rewards will be placed here
					before you send it to your in game warehouse.</p>";
				echo "<table width = '100%' cellpading = '0' cellspacing = '0' class = 'stats4'>";
						echo "<tr>";
						echo "<td>Item</td>";
						echo "<td>Action</td>";
					echo "</tr>";
					if(isset($_POST['submit'])) {
						if($sys->isOnline($_SESSION['user'])) {
							$sys->errorHandle("Please log out from the game...");
						} else {
							$sys->sendItemInGame($_POST['item'],$_POST['user']);
						}
					}
					$sys->onlineBank($_SESSION['user']);
					echo "</table>";
					echo "<div class = 'clear'></div>";
				break;
				
				case "my":
					echo "<p class = 'red'>Your items in warehouse that available for sale </p>";
					if(isset($_POST['submit'])) {
						if($sys->isOnline($_SESSION['user'])) {
							$sys->errorHandle("Please log out from the game...");
						} else {
							$sys->postItem($_POST['item_hex'],$_POST['seller'],$_POST['bidPrice'],$_POST['buyPrice']);
							Header("Location: market.php?p=items");
						}
					}
					$sys->listItems($_SESSION['user']);
				Break;
				
				case "items":
					echo "<p class = 'red'>Items posted for sale</p>";
					echo "<table width = '100%' cellpading = '0' cellspacing = '0' class = 'stats4' border = '1'>";
					echo "<tr>";
						echo "<td>Item</td>";
						echo "<td>Bids</td>";
						echo "<td>Buyout Price</td>";
						echo "<td>Seller</td>";
						echo "<td>Action</td>";
					echo "</tr>";
					if(isset($_POST['bid'])){
						if($sys->isOnline($_SESSION['user'])) {
							$sys->errorHandle("Please log out from the game...");
						} else {
							//$sys->placeBid($item,$id);
						}
					} elseif(isset($_POST['buy'])) {
						if($sys->isOnline($_SESSION['user'])) {
							$sys->errorHandle("Please log out from the game...");
						} else {
							$sys->buyItem($_POST['item'],$_POST['buyer'],$_POST['price1'],$_POST['price2']);
						}
					} elseif(isset($_POST['cancel'])){
						if($sys->isOnline($_SESSION['user'])) {
							$sys->errorHandle("Please log out from the game...");
						} else {
							$sys->cancelItem($_POST['item']);
						}
					}
					$sys->listItemsSale();
					echo "</table>";
					echo "<div class = 'clear'></div>";
				Break;
				
				default:
				Break;
			}
		}
}
?>
</div>
<?
include 'tpl/footer.php';