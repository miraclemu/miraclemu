<? 
session_start(); 
ob_start(); 
require_once 'engine/functions.php';
$sys = new system();
include 'tpl/header.php';
?>
<div class = "body">
	<h2>Characters Panel</h2>
	<p class = "red">In Order to perform any character action
	please log out from the game server, otherwise, system will block your actions.</p>
	<hr />
<? 
switch(@$_GET['action']) {
case "reset":
if(isset($_GET['name'])){
	if(!$sys->isOnline($_SESSION['user'])){
		$sys->MakeReset($sys->clean($_GET['name']),$sys->clean($_GET['NextReset']));
	}else {
		$sys->errorHandle("Please log out from the game");
	}

}
?>
<table width = "100%" cellpading = "0" cellspacing = "0" class = "stats2">
	<tr>
		<td>#</td>
		<td>Character Name</td>
		<td>Level</td>
		<td>Resets</td>
		<td>Zen</td>
		<td>Zen Cost</td>
		<td>Acc Experience</td>
		<td>Action</td>
	</tr>
	
	<? $sys->showCharacters($_SESSION['user']); ?>
	
</table>
<p> * Requirements for character reset 400 level + 1.000.000 * [Resets} Zen.</p>
<p> ^ Acc Experience shows how much account experience this action will give you. </p>
<p><a href = "characters.php" class ="title">Back To Characters Menu</a></p>
<?
Break;

case "pkclear":
if(isset($_POST['submit'])){
	if(!$sys->isOnline($_SESSION['user'])){
			$sys->pkClear($_POST['name']);
	} else{
		$sys->errorHandle("Please log out from the game");
	}
}
?>
<table width = "100%" cellpading = "0" cellspacing = "0"  class = "stats2">
	<tr>
		<td>#</td>
		<td>Character Name</td>
		<td>Level</td>
		<td>Zen</td>
		<td>Action</td>
	</tr>
	
	<? $sys->showCharacters4($_SESSION['user']); ?>
	
</table>
<p> * Requirements for character PK Clear - 100.000.000 Zen.</p>
<p><a href = "characters.php" class ="title">Back To Characters Menu</a></p>
<?
Break;

case "add" :
if(isset($_POST['submit'])){
	if(!$sys->isOnline($_SESSION['user'])){
		@$sys->addStats($_POST['name'],$_POST['str'],$_POST['dex'],$_POST['vit'],$_POST['ene'],$_POST['cmd']);
	}else {
		$sys->errorHandle("Please log out from the game");
	}
	
}
?>
<table width = "100%" cellpading = "0" cellspacing = "0"  class = "stats2">
	<tr>
		<td>#</td>
		<td>Character Name</td>
		<td>Free Points</td>
		<td>STR</td>
			<td>PTS</td>
		<td>AGI</td>
			<td>PTS</td>
		<td>VIT</td>
			<td>PTS</td>
		<td>ENE</td>
			<td>PTS</td>
		<td>CMD</td>
		<td>PTS</td>
		<td>Action</td>
	</tr>
	
	<? $sys->showCharacters2($_SESSION['user']); ?>
	
</table>
* Please add points once per character, otherwise you will just lose your points...
<p><a href = "characters.php" class ="title">Back To Characters Menu</a></p>
<?
Break;
case "Greset" :
if(isset($_GET['name'])){
	if(!$sys->isOnline($_SESSION['user'])){
		$sys->MakeGrandReset($sys->clean($_GET['name']),$sys->clean($_GET['NextGReset']));
	} else {
		$sys->errorHandle("Please log out from the game");
	}
}
?>
<table width = "100%" cellpading = "0" cellspacing = "0"  class = "stats2">
	<tr>
		<td>#</td>
		<td>Character Name</td>
		<td>Level</td>
		<td>Resets</td>
		<td>Zen</td>
		<td>Zen Cost</td>
		<td>Acc Experience</td>
		<td>Action</td>
	</tr>
	
	<? $sys->showCharacters3($_SESSION['user']); ?>
	
</table>
<p> * Requirements for character Grand reset 400 level + 100.000.000 * [Grand Resets} Zen.</p>
<p> ^ Acc Experience shows how much account experience this action will give you. </p>
<p><a href = "characters.php" class ="title">Back To Characters Menu</a></p>
<?
Break;
default:
?>

		<h2>Reset</h2>
			Reset your character level & stats and give you free levelUp points.
		<br />
		<a href = "characters.php?action=reset">
		<br />
		<div class = "red">Enter</div></a>

		<hr />
		
		<h2>Grand Reset</h2>
			Reset your character resets , level , stats & give you reward.

		<br />
		<a href = "characters.php?action=Greset">
		<br />
		<div class = "red">Enter</div></a>

		<hr />
		
		<h2>Add Points</h2>
			Spend levelUp points.<br />

		<a href = "characters.php?action=add">
		<br />
		<div class = "red">Enter</div></a>

		<hr />
		
		<h2>Reborn</h2>
		Reset your character level , stats, resets, grand resets & give you reward.
		<br />
		<a href = "characters.php?action=reborn">
		<br />
		<div class = "red">Enter</div></a>
		
		<hr />
		
		<h2>PK Clear</h2>
			Clears your PK level for zen.
		<br />
		<a href = "characters.php?action=pkclear">
		<br />
		<div class = "red">Enter</div></a>
		
		<hr />
		
		<h2>Reset Skill Tree</h2>
			Resets your sill tree points in case if you did a mistake.
		<br />
		<a href = "characters.php?action=clearpk">
		<br />
		<div class = "red">Enter</div></a>
	
<? 
Break;
}
?>	
</div>
<?
include 'tpl/footer.php';