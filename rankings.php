<?php 
session_start(); 
ob_start(); 
require_once 'engine/functions.php';
include 'tpl/header.php';
$sys = new system();
?>
<div class = "body">
<h2>Server Rankings</h2>
<a href = 'rankings.php' class = 'red'>General Rankings</a>
 / 
 <a href = 'rankings.php?order=money' class = 'red'>Money Rankings</a>
 / 
<a href = 'rankings.php?order=pk' class = 'red'>PvP Rankings</a> 
 / 
<a href = 'rankings.php?order=guild' class = 'red'>Guild Rankings</a>
<br /> <br />

<?
if(isset($_GET['order'])){
	switch($_GET['order']){
		
		case "money" :
			?>
			<table width = "100%" cellpading = '0' cellspacing = '1' class = "stats">
			<tr>
			<td>#</td>
			<td>Name</td>
			<td>Class</td>
			<td>Guild</td>
			<td>Level</td>
			<td>Resets</td>
			<td>Grand resets</td>
			<td>Reborns</td>
			<td>Zen</td>
			</tr>
					<? echo $sys->top150Money();?>
			</table>
			<?
		Break;
		
		case "pk" :
			?>
			<table width = "100%" cellpading = '0' cellspacing = '1' class = "stats">
			<tr>
			<td>#</td>
			<td>Name</td>
			<td>Class</td>
			<td>Guild</td>
			<td>Level</td>
			<td>Resets</td>
			<td>Grand resets</td>
			<td>Reborns</td>
			<td>Kills</td>
			</tr>
					<? echo $sys->top150Pk();?>
			</table>
			<?
		Break;
		
		case "guild" :
			?>
			<table width = "100%" cellpading = '0' cellspacing = '1' class = "stats">
			<tr>
			<td>#</td>
			<td>Name</td>
			<td>Score</td>
			<td>Members</td>
			<td>Owner</td>
			</tr>
				<? echo $sys->top150Guild();?>
			</table>
			<?
			
		Break;
		
		default:
		Break;
	}
} else{
?>
	<table width = "100%" cellpading = '0' cellspacing = '1' class = "stats">
		<tr>
			<td>#</td>
			<td>Name</td>
			<td>Class</td>
			<td>Guild</td>
			<td>Level</td>
			<td>Resets</td>
			<td>Grand resets</td>
			<td>Reborns</td>
			<td>Status</td>
		</tr>
			<? echo $sys->top150();?>
	</table>
<?
}
?>
</div>
<?php
include 'tpl/footer.php';